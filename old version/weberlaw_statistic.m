clear all
close all
clc

date = ['20151014'];
cd('C:\Users\jofan\Dropbox\hydrolab\expdata\weberlaw_20151014\analysis') ; % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir.
n_file = length(all_file) ; 
DataTime = zeros(1,n_file);
Phase = cell(1,n_file); %firing phase
Phasedelay = cell(1,n_file); %firing phase consider neuron delay
time = cell(1,n_file); %firing time 
timedelay = cell(1,n_file); %firing time consider neuron delay
weberf = cell(1,n_file);% weber's fraction
weberfdelay = cell(1,n_file);% weber's fraction consider neuron delay


for i=1:n_file
    clearvars -except all_file n_file i DataTime  weberf weberfdelay Phase Phasedelay time timedelay lumin lumindelay date
    file = all_file(i).name ;
    [pathstr, name, ext] = fileparts(file);
    directory = [pathstr,'\'];
    filename = [name,ext];
    load([filename]);
    if ~exist('theta','var');
        i
        continue
    end

    DataTime(i) = (TimeStamps(7) - TimeStamps(2))/5;

    Phase{i}=theta;
    Phasedelay{i}=thetadelay;
    time{i} = slocstime;
    timedelay{i} = slocstimedelay;
    weberf{i} = wf;
    weberfdelay{i} = wfdelay;  
%     lumin{i} = L;
%     lumindealy{i} = Ldelay;
    
    figure(1);hold on
    
    for j = 1:length(Phase{i})
        if Phase{i}(j)<=0.5 && j<=2 || Phase{i}(j)>=1.5 && j<=2
            plot(DataTime(i),Phase{i}(j),'r*')
        elseif 1.5>Phase{i}(j)>0.5 && j<=2
            plot(DataTime(i),Phase{i}(j),'b*')
        else
            plot(DataTime(i),Phase{i}(j),'g*')
        end
    end
    title([date,' Stimulus Period-Phase']);
    xlabel('Stimulus Period(s)');ylabel('Phase(pi)');
% % %     legend('on','off')
%     
    figure(2);hold on

    for j = 1:length(Phasedelay{i})
        if Phasedelay{i}(j)<=0.5 && j<=2 ||  Phasedelay{i}(j)>=1.5 && j<=2
            plot(DataTime(i),Phasedelay{i}(j),'r*')
        elseif 1.5>Phasedelay{i}(j)>0.5 && j<=2
            plot(DataTime(i),Phasedelay{i}(j),'b*')
        else
            plot(DataTime(i),Phasedelay{i}(j),'g*')
        end
    end
    title([date,' Consider ondely=0.14s offdelay=0.21s']);
    xlabel('Stimulus Period(s)');ylabel('PhaseDelay(pi)');
%     legend('on','off')
    
    figure(3);hold on
    for j = 1:length(weberf{i})
        if weberf{i}(j)<=0 
            plot(DataTime(i),weberf{i}(j),'r*')
        elseif weberf{i}(j)>0
            plot(DataTime(i),weberf{i}(j),'b*')
        else
            plot(DataTime(i),weberf{i}(j),'g*')
        end
    end
    title([date,' Weber Fraction']);
    xlabel('Stimulus Period(s)');ylabel('Weber Fraction');
%     legend('original','consider delay')
   
    figure(4);hold on
        for j = 1:length(weberfdelay{i})
            if weberfdelay{i}(j)<=0
                plot(DataTime(i),weberfdelay{i}(j),'r*')
            elseif weberfdelay{i}(j)>0
                plot(DataTime(i),weberfdelay{i}(j),'b*')
            else
                plot(DataTime(i),weberfdelay{i}(j),'g*')
            end
        end
    title([date,' Weber Fraction with Delay']);
    xlabel('Stimulus Period(s)');ylabel('Weber Fraction Delay');

    figure(5);hold on
    for j = 1:length(time{i})
        if time{i}(j)<=DataTime(i)/3
            plot(DataTime(i),time{i}(j),'r*');
        else
            plot(DataTime(i),time{i}(j),'b*');  
        end
     end
    title([date,' firing time']);
    xlabel('Stimulus Period(s)');ylabel('FiringTime(s)');
     
end

figure(1);hold off
figure(2);hold off
figure(3);hold off
figure(4);hold off
figure(5);hold off
saveas(figure(1),[date,'_phase_Ch',num2str(chst1),'to',num2str(chend1),'_Tr',num2str(trst),'to',num2str(trend),'.jpg'],'jpg');
saveas(figure(1),[date,'_phase_Ch',num2str(chst1),'to',num2str(chend1),'_Tr',num2str(trst),'to',num2str(trend),'.fig'],'fig'); 
saveas(figure(2),[date,'_phasedelay_Ch',num2str(chst1),'to',num2str(chend1),'_Tr',num2str(trst),'to',num2str(trend),'.jpg'],'jpg');
saveas(figure(2),[date,'_phasedelay_Ch',num2str(chst1),'to',num2str(chend1),'_Tr',num2str(trst),'to',num2str(trend),'.fig'],'fig');
saveas(figure(3),[date,'_weberf_Ch',num2str(chst1),'to',num2str(chend1),'_Tr',num2str(trst),'to',num2str(trend),'.jpg'],'jpg');
saveas(figure(3),[date,'_weberf_Ch',num2str(chst1),'to',num2str(chend1),'_Tr',num2str(trst),'to',num2str(trend),'.fig'],'fig');
saveas(figure(4),[date,'_weberfdelay_Ch',num2str(chst1),'to',num2str(chend1),'_Tr',num2str(trst),'to',num2str(trend),'.jpg'],'jpg');
saveas(figure(4),[date,'_weberfdelay_Ch',num2str(chst1),'to',num2str(chend1),'_Tr',num2str(trst),'to',num2str(trend),'.fig'],'fig');  
saveas(figure(5),[date,'_time_Ch',num2str(chst1),'to',num2str(chend1),'_Tr',num2str(trst),'to',num2str(trend),'.jpg'],'jpg');
saveas(figure(5),[date,'_time_Ch',num2str(chst1),'to',num2str(chend1),'_Tr',num2str(trst),'to',num2str(trend),'.fig'],'fig');  