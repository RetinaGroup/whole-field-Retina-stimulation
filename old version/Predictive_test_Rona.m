%Predictive_test
clear all
load('D:\rona\Google Drive\20160427\jitter1_20160427.mat')
SamplingRate=20000;

%%%%%%%%%%%%% TimeStamps %%%%%%%%%%%%%%
% if size(a_data,1)==1          
%     a_data2 = a_data(1,:);
% else
%     a_data2 = a_data(2,:);   
% end
% a_data2 = a_data2 - a_data2(1);
% [~,locs]=findpeaks(abs(diff(a_data2)),'MINPEAKHEIGHT',5*std(diff(a_data2))); %both positive and negative slope
% analog_loc = (locs)/SamplingRate;
% TimeStamps = analog_loc;

%%%Stimuli process
DataTime = max(TimeStamps); Start = min(TimeStamps);
isi = TimeStamps(2:end) - TimeStamps(1:end-1); isi = isi*1000;  %in ms
%plot(isi)
bin = 5;  BinningInterval = bin*10^-3;  %ms
back = 40;  steps = back*bin; %?*?ms-bin backwards
window = 10;  %ms of tolerance
current1 = 450;  %two comparing intervals
current2 = 550;
position1 = find(isi>current1-window & isi<current1+window); position1(find(position1<=back))=[];
position2 = find(isi>current2-window & isi<current2+window); position2(find(position2<=back))=[];
startend1 = [];
for i = 1:length(position1)
     temp = floor([sum(isi((1):position1(i))) sum(isi((1):position1(i)))+steps] / bin); %%+-
     startend1(i,:) = temp;
end
startend2 = [];
for i = 1:length(position2)
     temp = floor([sum(isi((1):position2(i))) sum(isi((1):position2(i)))+steps] / bin); %%+-
     startend2(i,:) = temp;
end
sets = min(size(startend1,1),size(startend2,1)); startend1 = startend1(1:sets,:); startend2 = startend2(1:sets,:);
%%%Spike process
BinningTime = [Start : BinningInterval : DataTime];
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60
    [n,xout] = hist(Spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end
[n,out] = hist(TimeStamps,BinningTime);
Stimuli = n;
BinningSpike(:,end) = 0;
%imagesc(BinningSpike)
%plot(BinningTime,sum(BinningSpike),BinningTime,30*Stimuli,'.')
%%%Predictive information
Neurons = sum(BinningSpike(1:60,:));
words1 = [];
for points = 1:size(startend1,1)
    words1 = [words1; Neurons(startend1(points,1):startend1(points,2))]; %%12 / 21
end
words2 = [];
for points = 1:size(startend1,1)
    words2 = [words2; Neurons(startend2(points,1):startend2(points,2))]; %%12 / 21
end

spiketrain1 = words1 + 1;    prob1 = sum(spiketrain1)/size(spiketrain1,1);
spiketrain2 = words2 + 1;    prob2 = sum(spiketrain2)/size(spiketrain2,1);
information = zeros(1,length(prob1)); %%%to be estimated
for d = 1:length(prob1)
    pw = 0.5*(prob1(d)+prob2(d));
    information(d) = 0.5*( prob1(d)*log(prob1(d)/pw) + prob2(d)*log(prob2(d)/pw) );
    %information(d) = 0.5*( prob1(d)*log(prob1(d)/pw) + prob2(d)*log(prob2(d)/pw) )/log(2);
end
figure
%%plot(-bin*length(information):bin:-bin,information)  %%predictive
plot(bin:bin:bin*length(information),information)  %%"memory"


