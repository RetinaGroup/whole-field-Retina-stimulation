%Predictive_test
clear all
load('C:\Users\HydroMatlab\Google ���ݵw��\20160520\jitter_lumin1.mat')
% load('C:\Users\HydroMatlab\Google ���ݵw��\20160520\diode\diode_jitterlumin2_0.1_20_3_20160520.mat')
SamplingRate=20000;


%%%%%%%%%%%%% TimeStamps %%%%%%%%%%%%%%
% if size(a_data,1)==1          
%     a_data2 = a_data(1,:);
% else
%     a_data2 = a_data(2,:);   
% end
% a_data2 = a_data2 - a_data2(1);
% [~,locs]=findpeaks(abs(diff(a_data2)),'MINPEAKHEIGHT',5*std(diff(a_data2))); %both positive and negative slope
% analog_loc = (locs)/SamplingRate;
% TimeStamps = analog_loc;


%%%Stimuli process
isi = a_data2(2,:);figure;plot(isi);
m = mean(isi(100:end)) % mV
tau=20; %stimu changing time(ms)
TimeStamps=[0:tau:300000];
DataTime = max(TimeStamps); Start = min(TimeStamps); %ms
bin = 50;  BinningInterval = bin*10^-3;  %ms
steps = 20;  
back = steps*bin; 
window = 0.1;  %tolerance
current1 = m-20;  %common future1
current2 = m+20;  %common future2
current3 = m-15;  %common future1
current4 = m+15;  %common future1
current5 = m-30;  %common future1
current6 = m-30;  %common future1

position1 = find(isi>current1-window & isi<current1+window); position1(find(position1<=back))=[];
position2 = find(isi>current2-window & isi<current2+window); position2(find(position2<=back))=[];
position3 = find(isi>current3-window & isi<current3+window); position3(find(position3<=back))=[];
position4 = find(isi>current4-window & isi<current4+window); position4(find(position4<=back))=[];
position5 = find(isi>current5-window & isi<current5+window); position5(find(position5<=back))=[];
position6 = find(isi>current6-window & isi<current6+window); position6(find(position6<=back))=[];

%%%% timewindow of common future %%%
startend1 = [];L1=[];
for i = 1:length(position1)
    startend1(i,:) = [position1(i)*1000/SamplingRate-back  position1(i)*1000/SamplingRate+back]; %unit:ms
    L1(i,:)=isi(position1(i)-5:position1(i)+5);
end

startend2 = [];L2=[];
for i = 1:length(position2)
    startend2(i,:) = [position2(i)*1000/SamplingRate-back  position2(i)*1000/SamplingRate+back]; %unit:ms    
    L2(i,:)=isi(position2(i)-5:position2(i)+5);
end

startend3 = [];L3=[];
for i = 1:length(position3)
    startend3(i,:) = [position3(i)*1000/SamplingRate-back  position3(i)*1000/SamplingRate+back]; %unit:ms    
end

startend4 = [];L4=[];
for i = 1:length(position4)
    startend4(i,:) = [position4(i)*1000/SamplingRate-back  position4(i)*1000/SamplingRate+back]; %unit:ms
    L4(i,:)=isi(position4(i)-5:position4(i)+5);
end

startend5 = [];L5=[];
for i = 1:length(position5)
    startend5(i,:) = [position5(i)*1000/SamplingRate-back  position5(i)*1000/SamplingRate+back]; %unit:ms
    L5(i,:)=isi(position5(i)-5:position5(i)+5);
end

startend6 = [];L6=[];
for i = 1:length(position6)
    startend6(i,:) = [position6(i)*1000/SamplingRate-back  position6(i)*1000/SamplingRate+back]; %unit:ms
    L6(i,:)=isi(position6(i)-5:position6(i)+5);
end

t=[-steps:tau:steps];
figure;plot(t,L1,'r',t,L2,'b');%plot common future

sets = min([size(startend1,1),size(startend2,1),size(startend3,1),size(startend4,1),size(startend5,1),size(startend6,1)]); 
startend1 = startend1(1:sets,:); startend2 = startend2(1:sets,:);
startend3 = startend3(1:sets,:);startend4 = startend4(1:sets,:);startend5 = startend5(1:sets,:);startend6 = startend6(1:sets,:);


%%%Spike process
BinningTime = [Start : BinningInterval : DataTime/1000];
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60
    [n,xout] = hist(Spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end
% [n,out] = hist(TimeStamps,BinningTime);
% Stimuli = n;
% BinningSpike(:,end) = 0;
figure;imagesc(BinningTime,[1:60],BinningSpike)
% figure;plot(BinningTime,sum(BinningSpike),BinningTime,30*Stimuli,'.')

%%%Predictive information

% Neurons = sum(BinningSpike(1:60,:));
for i=15
Neurons = BinningSpike(i,:);

    words1 = [];
    for points = 1:size(startend1,1)-20
        words1 = [words1; Neurons(ceil(startend1(points,1)/bin):ceil(startend1(points,2)/bin))]; % memory
        
    end
    
    
     words2 = [];
    for points = 1:size(startend1,1)-20
        words2 = [words2; Neurons(ceil(startend2(points,1)/bin):ceil(startend2(points,2)/bin))]; 
       
    end
    
      words3 = [];
    for points = 1:size(startend1,1)-20
        words3 = [words3; Neurons(ceil(startend3(points,1)/bin):ceil(startend3(points,2)/bin))]; 
        
    end
    
     words4 = [];
    for points = 1:size(startend1,1)-20
        words4 = [words4; Neurons(ceil(startend4(points,1)/bin):ceil(startend4(points,2)/bin))]; 
 
    end
    
      words5 = [];
    for points = 1:size(startend1,1)-20
        words5 = [words5; Neurons(ceil(startend5(points,1)/bin):ceil(startend5(points,2)/bin))]; 
    end
    
     words6 = [];
    for points = 1:size(startend1,1)-20
        words6 = [words6; Neurons(ceil(startend6(points,1)/bin):ceil(startend6(points,2)/bin))]; 

    end

spiketrain1 = sum(words1) ;    prob1 = hist(spiketrain1,0:1000:10000);%prob1 = sum(spiketrain1)/size(spiketrain1,1);
spiketrain2 = sum(words2) ;    prob2 = hist(spiketrain2,0:1000:10000);
spiketrain3 = sum(words3) ;    prob3 = hist(spiketrain3,0:1000:10000);
spiketrain4 = sum(words4) ;    prob4 = hist(spiketrain4,0:1000:10000);
spiketrain5 = sum(words5) ;    prob5 = hist(spiketrain5,0:1000:10000);
spiketrain6 = sum(words6) ;    prob6 = hist(spiketrain6,0:1000:10000);
information = zeros(1,length(prob1)); %%%to be estimated

for d = 1:length(prob1)
    pw = 1/6*(prob1(d)+prob2(d)+prob3(d)+prob4(d)+prob5(d)+prob6(d));
    information(d) = 1/6*( prob1(d)*log(prob1(d)/pw) + prob2(d)*log(prob2(d)/pw)+ prob3(d)*log(prob3(d)/pw)+ prob4(d)*log(prob4(d)/pw)+ prob5(d)*log(prob5(d)/pw)+ prob6(d)*log(prob6(d)/pw)+...
        (1-prob1(d))*log((1-prob1(d))/pw)+(1-prob2(d))*log((1-prob2(d))/pw)+(1-prob3(d))*log((1-prob3(d))/pw)+(1-prob4(d))*log((1-prob4(d))/pw)+(1-prob5(d))*log((1-prob5(d))/pw)+(1-prob6(d))*log((1-prob6(d))/pw))/log(2);
end
figure(i)
% plot(-bin*length(information):bin:-bin,information)  %%"memory"
% plot(bin:bin:bin*length(information),information)  %%predictive
plot(-back:bin:back,information)
end
datastrm
