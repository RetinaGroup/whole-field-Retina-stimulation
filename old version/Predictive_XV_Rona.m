%Predictive_cross
%using both x and v to calculate MI curve
clear all;
% close all
try
cd('E:\google_rona\20160909\jitterlumin2') ; % the folder of the files
catch
cd('C:\Users\jofan\Google ���ݵw��\20161007\m=0.1_diffr') ; % the folder of the files    
end
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir.
n_file = length(all_file); 
SamplingRate=20000;
cc=hsv(n_file);

for z =3
    clearvars -except all_file n_file z SamplingRate cc ey isi2 statispks statistime w fr
    file = all_file(z).name ;
    [pathstr, name, ext] = fileparts(file);
    directory = [pathstr,'\'];
    filename = [name,ext];
    load([filename]);
    name(name=='_')='-';
    %%%Stimuli process
    % isi = TimeStamps(2:end) - TimeStamps(1:end-1); isi = isi*1000;  %in ms
    SamplingRate=20000;
    a_data2=a_data(3,:);
    isi = a_data2(TimeStamps(1)*20000:TimeStamps(length(TimeStamps))*20000);
    meeen = mean(isi);
    DataTime = max(TimeStamps); Start = min(TimeStamps);
    bin = 50;  BinningInterval = bin*10^-3;  %ms
    shift = [1000 500];  %backward and forward shiftin ms
    roi = [49];
    states = [8 8];
    % plot(isi)

    %%%Spike process
    BinningTime = [Start : BinningInterval : DataTime];
    BinningSpike = zeros(60,length(BinningTime));
    for i = 1:60
        [n,xout] = hist(Spikes{i},BinningTime) ;
        BinningSpike(i,:) = n ;
    end
    [n,out] = hist(TimeStamps,BinningTime);
    Stimuli = n;
    BinningSpike(:,1) = 0;  BinningSpike(:,end) = 0;
    % imagesc(BinningTime(1,:),[1:60],BinningSpike(:,:));

    I=[];
    X = isi;
    nX = sort(X);
    temp=0;
    for j = 1:BinningInterval*SamplingRate:length(X)
        temp=temp+1;
        I(temp) = isi(j);
    end
    X=I;

    dI=[];
    temp=0;    
    for j=1:length(I)-1
        dI(j)=I(j+1)-I(j);
    end
    V=dI;

    nX = sort(X);
    abin = length(nX)/states(1);
    Xinter = [nX(1:abin:end) inf];
    nV = sort(V);
    abin = length(nV)/states(2);
    Vinter = [nV(1:abin:end) inf];
    for ss = 1:length(V)
        xx = find(X(ss)<Xinter,1)-1;
        vv = find(V(ss)<Vinter,1)-1;
        isi2(ss) = states(1)*(vv-1)+xx;
    end

    %%%Predictive information
    % Neurons = sum(BinningSpike(1:60,:));

    for nn=1:length(roi)
        n=roi(nn);
        Neurons = BinningSpike(n,1:length(isi2));

        backward = round(shift(1)/bin); forward=round(shift(2)/bin);
        dat = [];informationp=[];temp=backward+2;
        for i = 1:backward+1
            dat{i} = [Neurons((i-1)+forward+1:length(Neurons)-backward+(i-1))',isi2(forward+1:length(isi2)-backward)'];
            [N,C] = hist3(dat{i},[max(Neurons)+1,max(isi2)]);
            px = sum(N,1)/sum(sum(N));
            py = sum(N,2)/sum(sum(N));
            pxy = N/sum(sum(N));
            temp2 = [];
        for j = 1:length(px)
            for k = 1:length(py)
              temp2(k,j) = pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2) / (bin/1000);
            end
        end
        temp = temp-1;
        informationp(temp) = nansum(temp2(:)); 
        end

        dat = [];informationf = [];temp = 0;sdat = [];
    for i = 1:forward
            dat{i} = [Neurons(forward+1-i:length(Neurons)-backward-i)',isi2(forward+1:length(isi2)-backward)'];
            [N,C] = hist3(dat{i},[max(Neurons)+1,max(isi2)]);
            px = sum(N,1)/sum(sum(N));
            py = sum(N,2)/sum(sum(N));
            pxy = N/sum(sum(N));
            temp2 = [];
        for j = 1:length(px)
            for k = 1:length(py)
                temp2(k,j) = pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2) / (bin/1000);
            end
        end
        temp = temp+1;
        informationf(temp) = nansum(temp2(:)); 
    end
    information = [informationp informationf];

    t=[-backward*bin:bin:forward*bin];
    figure(n);hold on; plot(t,information,'color',cc(z,:),'LineWidth',2.5); set(gca,'FontSize',16)
    title(['Neuron',num2str(n),],'FontSize',10);
    xlabel('\delta t (ms)','FontSize',20);ylabel('MI (bits/s)','FontSize',20);

    end
end


