%Predictive_test
clear all
load('C:\Users\HydroMatlab\Google ���ݵw��\20160520\jitterlumin3_0.1_20_4_20160520_5.mat')
SamplingRate=20000;

%%%%%%%%%%%%% TimeStamps %%%%%%%%%%%%%%
% if size(a_data,1)==1          
%     a_data2 = a_data(1,:);
% else
%     a_data2 = a_data(2,:);   
% end
% a_data2 = a_data2 - a_data2(1);
% [~,locs]=findpeaks(abs(diff(a_data2)),'MINPEAKHEIGHT',5*std(diff(a_data2))); %both positive and negative slope
% analog_loc = (locs)/SamplingRate;
% TimeStamps = analog_loc;


%%%Stimuli process
f=20;
tau=1000/f; %stimu changing time(ms)
a_data2=(a_data-32768)*0.1042;
isi1 = a_data2(2,:);
isi=isi1(1:SamplingRate/f:end); % one point every change 
% figure;plot(isi);
m = mean(isi(100:end)) % mV
DataTime = 1000*length(a_data)/SamplingRate; Start = 0; %ms
bin = 25;  BinningInterval = bin*10^-3;  %ms
steps = 8;  
back = steps*bin; %ms
window = 1;  %tolerance
current=[m-10 m+10 m-20 m+20];

%%%% timewindow of common future %%%
position=[];startend = [];L=[];
for i=1:length(current)
    position{i} = find(isi>current(i)-window & isi<current(i)+window); position{i}(find(position{i}<=back))=[];
        startend{i}= [position{i}*tau-back ; position{i}*tau];startend{i}=startend{i}';%unit:ms
%     for j=1:length(position{i})
%         L{i}(j,:)=isi(position{i}(j)-5:position{i}(j));
%     end
    s(i) = size(startend{i},1);
end
%    t=[-5*tau:tau:0];
%     figure;plot(t,L{1},'r',t,L{2},'b');%plot common future
    
 sets = min(s); 
for i=1:length(current)
    startend{i} = startend{i}(1:sets,:); 
end

%%%Spike process
BinningTime = [Start : BinningInterval : DataTime/1000];
BinningSpike = zeros(60,length(BinningTime));
for i = 1:60
    [n,xout] = hist(Spikes{i},BinningTime) ;
    BinningSpike(i,:) = n ;
end
% [n,out] = hist(TimeStamps,BinningTime);
% Stimuli = n;
% BinningSpike(:,end) = 0;
% figure;imagesc(BinningTime,[1:60],BinningSpike)% figure;plot(BinningTime,sum(BinningSpike),BinningTime,30*Stimuli,'.')

%%%Predictive information

% Neurons = sum(BinningSpike(1:60,:));

for i=14
    Neurons = BinningSpike(i,:);

    words = {};
    for j=1:length(current)
        for points = 1:sets
            words{j}(points,:) = [Neurons(ceil(startend{j}(points,1)/bin):ceil(startend{j}(points,2)/bin))];
        end 
    end

    dat=[];information=[];
    for t=1:size(words{1},2)     
        temp = 0;
        for j=1:length(current)
            for k=1:points
                temp = temp+1;
                dat{t}(temp,:)=[words{j}(k,t) j];
            end
        end
        
%         sdat{t}=dat{t};
%         r=randperm(520);
%         for j=1:length(r)            
%                 sdat{t}(j,1)=dat{t}(r(j),1);
%         end
 
        [N,C]=hist3(dat{t},[max(dat{t}(:,1))+1,length(current)]); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:size(N,2)
            for k=1:size(N,1)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2);
            end
        end
        information(t)=nansum(temp2(:)); 
    end

figure(2);plot(0:bin:back,information,'*');
% plot(-bin*length(information):bin:-bin,information)  %%"memory"
% plot(bin:bin:bin*length(information),information)  %%predictive

end

