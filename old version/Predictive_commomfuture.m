T = 180;  dt = 1/60;  T = 0:dt:T;
X = zeros(1,length(T));
V = zeros(1,length(T));
G = 20; % damping  
w = 3*pi;%w = G/2/pi;  
D = 5; %dynamical range   
rate=1/dt;

for t = 1:length(T)-1
    X(t+1) = X(t) + V(t)*dt;
    V(t+1) = (1-G*dt)*V(t) - w^2*X(t)*dt + sqrt(dt*D)*randn;
end
X=X-mean(X)+0.15; %let mean(X)=0.1 lumin
figure;plot(T,X,'*');


window = 0.005;  %ms of tolerance
current1 = 0.13;  %common future1
current2 = 0.07;  %common future2
position1 = find(isi>current1-window & isi<current1+window); position1(find(position1<=back))=[];
position2 = find(isi>current2-window & isi<current2+window); position2(find(position2<=back))=[];

%%%% timewindow %%%
startend1 = [];L1=[];
for i = 10:length(position1)-1
    startend1(i,:) = [position1(i)*tau-back  position1(i)*tau+back]; %unit:ms
    L1(i,:)=a2(position1(i)-5:position1(i)+1);
end
startend2 = [];L2=[];
for i = 10:length(position2)-1
    startend2(i,:)=[position2(i)*tau-back  position2(i)*tau+back]; %unit:ms
    L2(i,:)=a2(position2(i)-5:position2(i)+1);
end
t=[-5*tau:tau:tau];
figure;plot(t,L1,'r',t,L2,'b');%plot common future