clearvars -except rate volt lumin_filter  L1 L2 enstrophy12 enstrophy1
daq_out = daqmx_Task('chan','Dev1/ao1' ,'rate',rate, 'Mode', 'f');
daq_out.write(0);
rate=20000;
% load('E:\rona\20160318\map');
%%%%%%%%%%%%% step3:calibration %%%%%%%%%%%%%%%%%%%
x=volt;
y=(lumin_filter)';

%% Signal %%%%%%%%%%%%%%%%%%
date = '20170524';
Tot = 300;  %300;  
dtau=0.06;  %for enstrophy12
T = dtau:dtau:Tot;
at = 30;  %30;
m = 0.05;
r = 0.0023;
L = r*enstrophy12(:,2);
%   L = 0.0006*enstrophy1(:,2); 
X = L-mean(L)+m; % X is the final stimuX
X = abs(X);
std(X)/m
ey1=zeros(1,length(X));
temp=X(1:Tot/dtau)';
temp2=repmat(temp,rate*dtau,1);
ey1=temp2(:)';

ey0=m*ones(1,at*rate);%REST
ey=[ey0 ey1];
a2=zeros(1,length(ey));
a2(at*rate:(at+1)*rate)=1;
a2((Tot+at-1)*rate:(Tot+at)*rate)=1;

t=[1/rate:1/rate:length(ey)/rate];
 figure(1);plot(t,ey);hold on
 figure;autocorr(ey1,500)

%%
eyf=ey;
ex = interp1(y,x,eyf,'spline');% ex=calibrate voltage
daq = daqmx_Task('chan','Dev1/ao1' ,'rate',rate, 'SampleNum', length(eyf));
% daq.stop
daq_out = daqmx_Task('chan','Dev1/ao0:1' ,'rate',rate, 'Mode', 'f');
daq_in = daqmx_Task('chan','Dev1/ai0' ,'rate',rate, 'SampleNum', length(eyf));

A=[];
A(:,1) = a2(1:length(eyf));
A(:,2) = ex(1:length(eyf));

%% output %%%%%%%%%%
daq_out.write(A); 
daq_in.read;
callumin=daq_in.data;
[b,a]=butter(2,10/rate,'low');
callumin_filter=filter(b,a,daq_in.data) ;
daq_in.wait;

close all;
daq_out = daqmx_Task('chan','Dev1/ao1' ,'rate',rate, 'Mode', 'f');
daq_out.write(0);

figure(5) ; plot(ex);title(['calibrated voltage v.s. time']);
figure(6) ; plot(callumin);title(['lumin v.s. time']);hold on;plot(ey,'r');plot(eyf,'g');

save(['G:\Rona\0exp\',date,'\diode\diode_tur_OL2_',date,'.mat'],'callumin','callumin_filter','a2');
