%for_rona_for_fun
clear
clc

load('E:\Dropbox\hydrolab (1)\HMM2_G=25_20161115.mat')
S = a_data(3,1:20:end);  %down sample to 1ms per element
S = S(30000:320000);  %remove weird parts
S = S-mean(S);  %zero mean
STA = (sin(2*pi*[1:300]/300));  %fake STA with ON signal at 300ms window
STA = STA(1:end/2);
F = conv(STA,S);
F = F(1:end-length(STA)+1);

Th = 2500;  %302500;
window_p = 800;
window_n = 800;
[a,b] = find(F>Th);
% temp = [];
% for i = 1:length(b)
%     if b(i)-window_p>0 && b(i)+window_n<length(F)
%         temp = [temp; F(b(i)-window_p:b(i)+window_n)];
%     else
%         continue
%     end
% end
% plot([-window_p:window_n],mean(temp))

bin = 1;
shift = [1000 1000];
Neurons = zeros(1,length(S));
Neurons(b) = 1;
isi2 = S;
backward=round(shift(1)/bin); forward=round(shift(2)/bin);
dat=[];informationp=[];temp=backward+2;
    
for i = 1:backward+1
    dat = [Neurons((i-1)+forward+1:length(Neurons)-backward+(i-1))',isi2(forward+1:length(isi2)-backward)'];
    [N,C] = hist3(dat,[20,20]);    
%     [N,C]=hist3(dat,[floor(max(Neurons)+1),max(isi2)]); %20:dividing firing rate  6:# of stim
    px = sum(N,1)/sum(sum(N));
    py = sum(N,2)/sum(sum(N));
    pxy = N/sum(sum(N));
    temp2 = [];
    for j = 1:length(px)
        for k = 1:length(py)
          temp2(k,j) = pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/(bin/1000);
        end
    end
    temp = temp-1;
    informationp(temp) = nansum(temp2(:)); 
end

dat = [];informationf = [];temp = 0;sdat = [];
for i = 1:forward
%     dat{i} = [Neurons(forward+1:length(Neurons)-backward)' , isi2(forward+1+(i-1):length(isi2)-backward+(i-1))'];
    dat = [Neurons(forward+1-i:length(Neurons)-backward-i)',isi2(forward+1:length(isi2)-backward)'];
    [N,C] = hist3(dat,[20,20]);
%     [N,C]=hist3(dat,[floor(max(Neurons)+1),max(isi2)]); %20:dividing firing rate  6:# of stim   %max(Neurons)+1,max(isi2)
    px = sum(N,1)/sum(sum(N)); % x:stim
    py = sum(N,2)/sum(sum(N)); % y:word
    pxy = N/sum(sum(N));
    temp2 = [];
    nor = sum(Neurons(forward+1-i:length(Neurons)-backward-i));
    for j = 1:length(px)
        for k =1 :length(py)
            temp2(k,j) = pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2)/(bin/1000);
        end
    end
    temp = temp+1;
    informationf(temp) = nansum(temp2(:)); 
end
information = [informationp informationf];

t = [-backward*bin:bin:forward*bin];
% figure; 
plot(t,information,'color',[.47 .67 .19],'LineWidth',2.5); set(gca,'FontSize',16)
xlabel('\delta t(ms)','FontSize',20);ylabel('mutual information','FontSize',20);


