clear all

cd('C:\Users\jofan\Dropbox\hydrolab\stimulus\5aset')
all_file = dir('*.dat') ; % change the type of the files which you want to select, subdir or dir.
n_file = length(all_file) ; 


for m=1
    clearvars -except m all_file n_file ;
    file = all_file(m).name ;
    [pathstr, name, ext] = fileparts(file);
    directory = [pathstr,'\'];
    filename = [name,ext];
    file = strcat([directory,filename]);
    a =importdata(file);
% 
%     id = fopen('x.dat', 'w'); %直接覆蓋
    id = fopen([filename(1:end-4),'.dat'], 'w'); %另存

%%%  存入文字部分  %%%
for x=1:size(a.textdata,1)
    fprintf(id,'%s\t%s\n',a.textdata{x,:});
end

%%%  存入數據部分  %%%
a.data(:,1)=a.data(:,1)+2000;
for x=1:size(a.data,1)
    fprintf(id,'%d\t%d\n',a.data(x,:));
end
fclose(id);
end