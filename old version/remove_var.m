clear all
close all
clc

cd('D:\rona\Google Drive\20160520') ; % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir.
n_file = length(all_file) ; 
for m =1
    file = all_file(m).name ;
    [pathstr, name, ext] = fileparts(file);
    directory = [pathstr,'\'];
    filename = [name,ext];
    rmvar([filename],'a_data');
    load([filename]);
end