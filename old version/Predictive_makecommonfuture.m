T = 3000;  dt = 1/20;  T = 0:dt:T;
X = zeros(1,length(T));
V = zeros(1,length(T));
G = 20; % damping  
w = 3*pi;%w = G/2/pi;  
D = 5; %dynamical range   
rate=1/dt;

for t = 1:length(T)-1
    X(t+1) = X(t) + V(t)*dt;
    V(t+1) = (1-G*dt)*V(t) - w^2*X(t)*dt + sqrt(dt*D)*randn;
end

X=X-mean(X)+0.1; %let mean(X)=0.1 lumin
X=abs(X);
% figure;plot(T,X,'*');

tau=1000/50; %stimu changing time(ms)
TimeStamps=[0:tau:210000];
bin = 5;  BinningInterval = bin*10^-3;  %ms
back = 100;  %ms
window = 0.0001;  %tolerance
position=[];
for i=1:10
    current(i)=0.01+(i-1)*0.02;
    position{i} = find(X>current(i)-window & X<current(i)+window);
end
% current1 = 0.13;  %common future1
% current2 = 0.07;  %common future2
% position1 = find(X>current1-window & X<current1+window); position1(find(position1<=back))=[];
% position2 = find(X>current2-window & X<current2+window); position2(find(position2<=back))=[];

%%%% timewindow %%%
startend = []; L = [];
figure;
for i=1:10
    colors = rand(1,3);
    for j = 1:length(position{i})-1
        L{i,j}=X(position{i}(j)-5:position{i}(j)+5);
        plot(L{i,j},'color',colors);hold on
    end
end

next=[];Lt=[];
rand=randperm(10);

    for i=1:length(position{rand(2)})-1
        if L{rand(1),1}(end)-0.005<L{rand(2),i}(1)< L{rand(1),1}(end)+0.005
            next(end+1)=i;
        end
    end
L1 = L{rand(1),1};
n=randperm(length(next),1);
L2 = L{rand(2),next(n)};
Lt =[L1 L2];

for j=3:10
    for i=1:length(position{rand(j)})-1
        if Lt(end)-0.005 < L{rand(j),i}(1) < Lt(end)+0.005
            next(end+1)=i;
        end
        n=randperm(length(next),1);
        Lt = [Lt L{rand(j),next(n)}];
    end
end




