% Combine pictures into a video, by Rona, 2017
clear all; close all; clc;
cd('E:\naturesce');
pic=dir('*.jpg');
b = repmat([1,2,3,4,5,6,7,8,9,10],1,10);
temp = randperm(100);
for i=1:length(b)
    im(:,:,:,2*i)=imread(strcat(num2str(b(temp(i))),'.jpg'));
    M(i) = getframe;
end
for k = 2:2:200
    flick = zeros(2000,3008);
    for i = 1:50:2000
        for j = 1:50:3008
            flick(i:i+50,j:j+50) =  round(rand);
        end
    end
    flick=flick(1:3008,1:2000);
    im(:,:,:,k)=flick; 
end

movie2avi(M,'out.avi','FPS',10)