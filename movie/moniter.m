% Make whole field intensity stimulation video, by Rona, 2017
clear all
close all
load('E:\google_rona\monitormap.mat')
x=level;
y=luminmap;
rate = 200;

%% HMM 
load('\\192.168.0.100\experiment\Neuron\rona\noise\normalnoise.mat')
Tot = 300;  
dt = 50*10^-3; 
dtau=50*10^-3; %step width 
%     T = dtau:dtau:Tot;
T = dt:dt:Tot;
G = 5; % damping  
w = G/(2*1.06);%w = G/(2w)=1.06;  
D = 4; %dynamical range
at = 30;
r = 0.04;
m = 0.24;

a2=[];ey=[];ey1=[];
L = zeros(1,length(T));
V = zeros(1,length(T));
for t = 1:length(T)-1
    L(t+1) = L(t) + V(t)*dt;  %(th*(mu - X(t)))*dt + sqrt(dt)*randn; th = .01;  mu = 3;
%     V(t+1) = (1-G*dt)*V(t) - w^2*L(t)*dt + sqrt(dt*D)*randn;  
    V(t+1) = (1-G*dt)*V(t) - w^2*L(t)*dt + sqrt(dt*D)*noise(t);
end      
L = r*L;
X = L-mean(L)+m; % X is the final stimuX
X = abs(X);
std(X)/m
ey1=zeros(1,length(X));
temp = X(1:dtau/dt:Tot/dt);
temp2=repmat(temp,rate*dtau,1);
ey1=temp2(:)';
ey0=m*ones(1,at*rate);%REST
ey=[ey0 ey1];

t=[1/rate:1/rate:length(ey)/rate];
figure(1);plot(t,ey);hold on
% figure(2);autocorr(ey1,20);
%% OU
% load('\\192.168.0.100\experiment\Neuron\rona\noise\normalnoise.mat')
% Tot = 300;  
% dt = 50*10^-3; 
% dtau=50*10^-3; %step width 
% T = dtau:dtau:Tot;
% tau=0.05;
% D = 4; %dynamical range
% at = 30;
% r = 0.023;
% m=0.27;
% a2=[];ey=[];ey1=[];
% L = zeros(1,length(T));
% for i = 1:length(T)-1
% %     L(i+1) = L(i) + (-L(i)/tau + randn*sqrt(D/dt))*dt;
%     L(i+1) = L(i) + (-L(i)/tau + noise(i)*sqrt(D/dt))*dt;
% end      
% L = r*L;
% X = L-mean(L)+m; % X is the final stimuX
% X = abs(X);
% for i=1:length(X)
% ey1(rate*dtau*(i-1)+1:rate*dtau*i)=X(i);
% end
% std(X)
% ey0=m*ones(1,at*rate);%REST
% ey=[ey0 ey1];
% 
% t=[1/rate:1/rate:length(ey)/rate];
% figure(3);plot(t,ey);hold on 

%% Contrast Mapping
% T = 180; %nth in sec
% unit = 0.050;  %time step unit in ~ms
% steps = T/unit;
% r=0.1;
% d = r*(rand(1,steps)-0.5);  %intensity
% at=10;
% m=0.24;
% ey1=[];ey=[];ey0=[];
% for i=1:steps
%     ey1(rate*unit*(i-1)+1:rate*unit*i)=m+d(i);
% end
% ey0=m*ones(1,at*rate);%REST
% ey=[ey0 ey1]; 
% figure;plot(ey);
% a2=[0*ones(1,at*rate) ey1];

%% onoff %%%%%%%%%%%
repeat = 1;
rt = 5; % rest time
OT =  10*10^-3; % dim time
T = 10*10^-3; % bright time
ns = 500; % # of  step in a loop
period = rt+ns*(T+OT); % period of a loop
m = 0;% onoff 0-0.18
d = 255;% increment of lumin
x1 = 1:period*rate*repeat;
ey = zeros; 
a2 = -0.1*ones;
at = 3;%adaptation time
aL=(m+d)/2;
ey(1:at*rate)=aL;

for i = 1:repeat 
    ey(at*rate+period*rate*(i-1)+1:at*rate+period*rate*(i-1)+rt*rate)= aL; %前五秒
    a2(at*rate+period*rate*(i-1)+1:at*rate+period*rate*(i-1)+rt*rate)=-0.1;
    for j = 1:ns % how many a2 in a trial      
        ey(at*rate+period*rate*(i-1)+rt*rate+(j-1)*(T+OT)*rate+1:at*rate+period*rate*(i-1)+rt*rate+(j-1)*(T+OT)*rate+T*rate)=m+d;
        ey(at*rate+period*rate*(i-1)+rt*rate+(j-1)*(T+OT)*rate+T*rate+1:at*rate+period*rate*(i-1)+rt*rate+(j-1)*(T+OT)*rate+T*rate+OT*rate)=m;
        a2(at*rate+period*rate*(i-1)+rt*rate+(j-1)*(T+OT)*rate+1:at*rate+period*rate*(i-1)+rt*rate+(j-1)*(T+OT)*rate+T*rate)=1;%如果每個sine都有timestamps          
        a2(at*rate+period*rate*(i-1)+rt*rate+(j-1)*(T+OT)*rate+T*rate+1:at*rate+period*rate*(i-1)+rt*rate+(j-1)*(T+OT)*rate+T*rate+OT*rate)=-0.1;
    end

end
x1 = 1:length(ey);
figure;plot(x1,ey);
%%
% ex = interp1(y,x,ey,'nearest');% ex=calibrate voltage
ex = [];
for j=1:length(ey)
    yq=ey(j);
    for i = 1:length(y)-1
        if (y(i) <= yq && yq < y(i+1)) || (y(i) > yq && yq >= y(i+1))
            ex(:,j) = interp1([y(i),y(i+1)],[x(i),x(i+1)],yq);
        end
    end
end
%%
ex=ey;
D = [];mov=[];
l=20;w=40;
k = length(ex);
D=ones(l,w,3,k+21);
for i=1:k
    D(:,:,1,i+20) = ey(i)*ones(l,w);
    D(:,:,2,i+20) = ey(i)*ones(l,w);
    D(:,:,3,i+20) = ey(i)*ones(l,w);
end

D(:,:,1,k+21)=0.001*ones(l,w);
D(:,:,2,k+21)=0.001*ones(l,w);
D(:,:,3,k+21)=0.001*ones(l,w);

for i=1:10
    D(:,:,1,i) = 0.001*ones(l,w);
    D(:,:,2,i) = 0.001*ones(l,w);
    D(:,:,3,i) = 0.001*ones(l,w);
end
mov = immovie(D);
% implay(mov);

%%
D = [];mov=[];
l=20;w=40;
k = 500;
D=ones(l,w,1,k);

for i=2:2:k
    D(:,:,1,i)=255*ones(l,w);
end
mov = immovie(D,gray);
implay(mov);

%%
myVideo = VideoWriter('E:\60fps.avi');
myVideo.FrameRate = 60;
open(myVideo);
writeVideo(myVideo, mov);
close(myVideo);
% save('E:\HMM_G=05_20170308.mat','ey')