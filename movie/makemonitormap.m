% Make a linear light increasing video, by Rona, 2017
D = [];mov=[];
k = 1500;
D=ones(200,200,3,300+40);
for i=5:5:k
    D(:,:,1,i/5+20) = i/1500*ones(200,200);
    D(:,:,2,i/5+20) = i/1500*ones(200,200);
    D(:,:,3,i/5+20) = i/1500*ones(200,200);
end
D(:,:,1,k/5+40)=1/1500*ones(200,200);
D(:,:,2,k/5+40)=1/1500*ones(200,200);
D(:,:,3,k/5+40)=1/1500*ones(200,200);
mov = immovie(D);
implay(mov);
myVideo = VideoWriter('E:\map_5.avi');
myVideo.FrameRate = 20;
open(myVideo);
writeVideo(myVideo, mov);
close(myVideo);


% 
%  A=ones(10000,1000,3);
%  for i = 1:10000
%      A(i,:,1)=0.0001*i;
%      A(i,:,2)=0.0001*i;
%      A(i,:,3)=0.0001*i;
%  end
%  figure;imshow(A)
%  
%  B=ones(1000,1000,1);
%  for i=1:1000
%      B(i,:,1)=i*0.001;
%  end
%  figure;imshow(B,gray)