% Analysis continuous three sine wave stimulation, by Rona, 2016
clear all
close all
clc

cd('C:\Users\hydrorecord\Google Drive\20160229\3setsine') ; % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir.
n_file = length(all_file) ; 

%%%%%%%%%%%%%%   user's setting  %%%%%%%%%%%%%%%%%%%%%%
BinningInterval=0.005;
SamplingRate=20000;
chst1=30;chend1=60;%focus on channel chst1 to chend1
chst2=43;chend2=48;
trst=2;trnum=20; %sweep (trnum) trails from (trst)
a=40;b=10;
for m = 3
    clearvars -except a b all_file n_file m BinningInterval SamplingRate chst1 chend1 chst2 chend2 trst trnum
    file = all_file(m).name ;
    [pathstr, name, ext] = fileparts(file);
    directory = [pathstr,'\'];
    filename = [name,ext];
    load([filename]);
    name(name=='_')='-';
    
    
%%%%%%%%%%%%%%%%%%%%%  TimeStamps  %%%%%%%%%%%%%%%%%%%
    if size(a_data,1)==1              %only find one analog channel, possibly cause by the setting in MC_rack
        a_data2 = a_data(1,:);
    else
        a_data2 = a_data(2,:);   % analog=A3:a_data(3,:) ;analog=A2:a_data(2,:) 
    end
    a_data2 = a_data2 - a_data2(1);
    [~,locs]=findpeaks(diff(-a_data2),'MINPEAKHEIGHT',5*std(diff(a_data2)));
    analog_loc = (locs)/SamplingRate;
    TimeStamps2 = analog_loc;
%%%%%%%%%%%%%%%%%%%%%%%%%%  Binning  %%%%%%%%%%%%%%%%%%%%%%%%%
%     TimeStamps2=TimeStamps(1:2:length(TimeStamps)); %if there are 2 timestamps in one trail
    if length(TimeStamps)<=(trst+trnum-1)
        trnum = length(TimeStamps)-trst+1;
    end
%     trend=trst+trnum-1;
    trend=length(TimeStamps);
    DataTime = (TimeStamps(3) - TimeStamps(2));
    T = TimeStamps2(2)-TimeStamps(2);
    BinningTime = [ 0 : BinningInterval : DataTime];
    
    %     cut_spikes = seperate_trials(Spikes,TimeStamps(trst:trend)); %%% forcus on which trails   
%     BinningSpike = zeros(60,length(BinningTime));
%     for i = 1:60
%         [n,xout] = hist(cut_spikes{i},BinningTime) ;
%         BinningSpike(i,:) = n ;
%     end
    
    repeat = 1;
    rt=5; % rest time
%     T = 1; % period of sine 
    ns=3; % # of  sine waves of a loop
    period=rt+ns*T; % period of a loop
    x1 = 0:BinningInterval:period;
   
    for i = 1:repeat 
        y(period*(i-1)+1:ns*T*i/BinningInterval)=5*sin(2*pi*x1(1:ns*T/BinningInterval)/T)+6;
        y(ns*T*i/BinningInterval:i*period/BinningInterval)=6;
    end
%     x = BinningInterval:BinningInterval:period;
    x = 0:BinningInterval:period;
 
  
% %%%%%%%%%%%%%%%%%%%%%%%%%  Plot Different Trials   %%%%%%%%%%%%%%%%%
%     if length(TimeStamps2)<=15
%         sweepend=length(TimeStamps2)-1;
%     else
%         sweepend=15;
%     end
    sweepend=trend;
    figure(2);
    set(gcf,'position',[150,30,1024,900])
    h = subplot(sweepend+1,1,1);
    
    for sweepindex=1:sweepend-1
        TimeStampsSweep=TimeStamps(sweepindex:sweepindex+1); % forcus on which trails 
        cut_spikes = seperate_trials(Spikes,TimeStampsSweep);      
        for i = 1:60
            [n,xout] = hist(cut_spikes{i},BinningTime) ;
            BinningSpike(sweepindex,i,:) = n ;
        end

        subplot(sweepend+1,1,sweepindex);
        plot(BinningTime,squeeze(sum(BinningSpike(sweepindex,chst1:chend1,:),2)));
    end     
        
    subplot(sweepend+1,1,sweepindex+1);
    plot(x,y,'r');
%     plot(TimeStamps(1:2)-TimeStamps(1), ones(1,length(2-1+1)),'r^')
    samexaxis('abc','xmt','on','ytac','join','yld',1) % function need ''parseArgs'' function, from matlab center
%         h = get(gcf,'children');
    set(get(h,'title'),'string',[name,'  ch',num2str(chst1),' to ',num2str(chend1)]);
%     set(get(h,'title'),'string',[name,'  ch',num2str(chst1),' to ',num2str(chend1)],'units','normalized','position',[0.5,1.0]);  

    saveas(gcf,[name,'_trials.jpg'],'jpg');
    saveas(gcf,[name,'_trials.fig'],'fig');  

%%%%%%%%%%%%%%%%% raster plot %%%%%%%%%%%%%%%%%%%
    BinningSpike2 = sum(BinningSpike(trst:trend-1,:,:),1);
    figure(1)
    imagesc(BinningTime,[1:60],squeeze(BinningSpike2));
    name(name=='_')=' ';
    title([name]);
    title(['T=', sprintf('%.3f',T),'s', '(sum over trails)  Trail ',num2str(trst),' to',num2str(trend-1)]);  
    xlabel('t(s)');ylabel('channel');
    colorbar;
    saveas(gcf,[name,'_raster.jpg'],'jpg')
    saveas(gcf,[name,'_raster.fig'],'fig')    
%%%%%%%%%%%%%  get peaks  %%%%%%%%%%%%%%%%%%%%%
    trnum = trend-trst;
    SB=sum(BinningSpike2(1,chst1:chend1,:),2);
    SB1=squeeze(SB)/trnum;  
%     SB1=sum(BinningSpike(chst1:chend1,:),1)/trnum;      
    [spks,slocs]=findpeaks(SB1,'minpeakdistance',floor(DataTime/BinningInterval/a),'MINPEAKHEIGHT',b);    

    if isempty(slocs)
        slocstime=NaN;
    end
    
    theta=zeros;
    wf=zeros;
    slocstime=(slocs-1)*BinningInterval;    
    for i=1:length(slocstime) 
        if slocstime(i)<=ns*T 
            theta(i)=2*(mod(slocstime(i),T)/T);%firing phase(unit:pi)
            wf(i)=(y(slocs(i))-y(slocs(i)-1))./((y(slocs(i))+y(slocs(i)-1))/2)/(2*BinningInterval);  %%weber's fraction   
        else
            theta(i)=NaN;
            wf(i)=NaN;
        end
    end
    
%%%%%%%%%%%%%%%%%%%%%%%%%% consider delay time %%%%%%%%%%%%%%%%%
    ondelay=0.07;offdelay=0.22;
    slocstimedelay=[];
%     for i=1:length(slocstime)
%         if slocstime(i)<=0.25*T+ondelay || slocstime(i)>=0.75*T+offdelay
%             slocstimedelay(i)=slocstime(i)-ondelay;
%         else
%             slocstimedelay(i)=slocstime(i)-offdelay; 
%         end
%     end
    for i=1:length(slocstime)
        if theta(i)<=0.5+ondelay/T*2 || slocstime(i)>=0.75+offdelay/T*2 % unit:pi
            slocstimedelay(i)=slocstime(i)-ondelay;
        else
            slocstimedelay(i)=slocstime(i)-offdelay; 
        end
    end     
        
    
%%%%%%%%%%%%%%%%%%%    statistics    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    slocstimedelay(slocstimedelay<0) = slocstimedelay(slocstimedelay<0)+DataTime;
    thetadelay=zeros;
    wfdelay=zeros;
    slocsdelay=ceil(slocstimedelay/BinningInterval+1);            
    for i=1:length(slocstime)
        if slocstimedelay(i)<=ns*T
            thetadelay(i)=2*(mod(slocstimedelay(i),T)/T);%firing phase(unit:pi)
            wfdelay(i)=(y(slocsdelay(i))-y(slocsdelay(i)-1))./((y(slocsdelay(i))+y(slocsdelay(i)-1))/2)/(BinningInterval);  %%weber's fractio
        else
            thetadelay(i)=NaN;
            wfdelay(i)=NaN;
        end
    end
    
     
        
%%%%%%%%%%%%%%%%%%%%%%%%%  Plot histogram   %%%%%%%%%%%%%%%%%        
    figure(3)
    subplot(6,1,2:5); 
    plot(BinningTime,SB1,slocstime,spks,'r*'); 
    title(['T=', sprintf('%.3f',T),'s','   Ch',num2str(chst1),' to ',num2str(chend1),'   Trail ',num2str(trst),' to',num2str(trend-1),sprintf('\n'),...
           ' PeakTime(s)=',sprintf('%8.3f',slocstime),sprintf('\n'),'PeakPhase(pi)=',sprintf('%8.3f',theta),sprintf('\n'),'Weber Fraction=',sprintf('%8.3f',wf),sprintf('\n'),...
           'dePeakTime(s)=',sprintf('%8.3f',slocstimedelay),sprintf('\n'),'dePeakPhase(pi)=',sprintf('%8.3f',thetadelay),sprintf('\n'),'deWeberFration=',sprintf('%8.3f',wfdelay)]);  
    ylabel('firing rate per 5ms per trail');
    subplot(6,1,6);
    plot(x,y,'r');
    xlabel('t(s)');
    samexaxis('abc','xmt','on','ytac','join','yld',1)
   
    saveas(gcf,[name,'_Ch',num2str(chst1),'to',num2str(chend1),'_Tr',num2str(trst),'to',num2str(trend-1),'_hist.jpg'],'jpg')
    saveas(gcf,[name,'_Ch',num2str(chst1),'to',num2str(chend1),'_Tr',num2str(trst),'to',num2str(trend-1),'_hist.fig'],'fig')
      
    
    save(filename,'T','slocstime','slocstimedelay','wf','wfdelay','theta','thetadelay','-append'); % save data into original dat

    
%     catch
%         [msgstr,msgerr] = lasterr;
%         disp([msgstr,msgerr])
%     end
       
%%%%%%%%%  focus on other channels   %%%%%%%%%%%%%%    
%     clearvars spks slocs slocstime theta 
%     SB2=sum(BinningSpike(c:d,:))/length(TimeStamps);    
%     [spks,slocs]=findpeaks(SB2,'minpeakdistance',floor(length(BinningTime)/4),'MINPEAKHEIGHT',0.2);   
%     slocstime=(slocs-1)*BinningInterval;    
%     theta=slocs/length(BinningTime)*2; %firing phase(unit:pi)
%      wf=(y(slocs(1)+1)-y(slocs(1)-1))/(2*BinningInterval)/((y(slocs(1)+1)+y(slocs(1)-1))/2);  %%weber's fraction
%     figure
%     plot(BinningTime,SB2,slocstime,spks,'r*');
%     title(['T=', sprintf('%.3f',DataTime),'s','    Ch',num2str(c),' to ',num2str(d),sprintf('\n'),' PeakTime(s)=',sprintf('%8.3f',slocstime),sprintf('\n'),...
%         'PeakPhase(pi)=',sprintf('%8.3f',theta),sprintf('\n'),'Weber Fraction=',sprintf('%.3f',wf)]);
%     xlabel('t(s)');ylabel('spike# in 60 channels within 5ms');
%     saveas(gcf,['T=', num2str(DataTime),'s',' Ch',num2str(a),'to',num2str(b),'_hist.jpg'],'jpg')
%     saveas(gcf,['T=', num2str(DataTime),'s',' Ch',num2str(a),'to',num2str(b),'_hist.fig'],'fig')  
        
end