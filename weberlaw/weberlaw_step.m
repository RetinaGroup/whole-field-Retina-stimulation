% Analysis step stimulation, by Rona, 2016
% clear all
% close all
% clc

cd('E:\google_rona\20161201\step_diffm') ; % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir.
n_file = length(all_file) ; 

%%%%%%%%%%%%%%   user's setting  %%%%%%%%%%%%%%%%%%%%%%
BinningInterval=0.001;
SamplingRate=20000;
chst1=1;chend1=60;%focus on channel chst1= to chend1
chst2=38;chend2=45;
trst=2;trnum=28; %sweep trail from trst to trend
f=50;p=8;

for m=1:5
%     for chst1=1:60
%         chend1=chst1;

    clearvars -except all_file n_file m BinningInterval SamplingRate chst1 chend1 chst2 chend2 trst trnum f p ch chst test max min statispks statistime
    file = all_file(m).name ;
    [pathstr, name, ext] = fileparts(file);
    directory = [pathstr,'\'];
    filename = [name,ext];
    load([filename]);
    name(name=='_')='-';

    min=0.02;
    max=0.12;
%     So = xlsread('C:\Users\hydrossd\Desktop\20160311-2016-03-13\20160311_sort',1);
%     ch = unique(So(:,1));
%     load(['C:\Users\jofan\Google ���ݵw��\20160719\diode\','diode_',filename]);
%%%%%%%%%%%%%%%%%%%%%%  TimeStamps  %%%%%%%%%%%%%%%%%%%
    if size(a_data,1)==1              %only find one analog channel, possibly cause by the setting in MC_rack
        a_data2 = a_data(1,:);
    else
        a_data2 = a_data(3,:);   % analog=A3:a_data(3,:) ;analog=A2:a_data(2,:) 
    end
    a_data2 = a_data2 - a_data2(1);
%     a_data = diff(a_data);
    [~,locs]=findpeaks(diff(a_data2),'MINPEAKHEIGHT',5*std(diff(a_data2)));
% %      [~,locs]=findpeaks(diff(a_data2),'MINPEAKHEIGHT',50000);
    analog_loc = (locs)/SamplingRate;
% %     TimeStamps = cat(2,TimeStamps, analog_loc);   
    TimeStamps = analog_loc;
% %     for i=1:23
% %         TimeStamps(i)=25.31+(i-1)*5.5;
% %     end

%%%%%%%%%%%%%%%%%%%%%%%%%%  Binning  %%%%%%%%%%%%%%%%%%%%%%%%%
    TimeStamps2=TimeStamps(1:1:length(TimeStamps)); 
    if length(TimeStamps2)<=(trst+trnum-1)
        trnum = length(TimeStamps2)-trst+1;
    end
    trend=trst+trnum-1;
 
    DataTime = (TimeStamps2(2) - TimeStamps2(1));
    delT = (TimeStamps2(2) - TimeStamps2(1) - 5.5);

    cut_spikes = seperate_trials(Spikes,TimeStamps2(trst:trend));    

    BinningTime = [ 0 : BinningInterval : DataTime];
    %%%%%% a3 %%%%%%%%%
%     x1 = 0:BinningInterval:DataTime-BinningInterval;
%     y1=[]; 
%     y1(1:1/BinningInterval)=0.6; %(unit:nA)
%     y1(1/BinningInterval+1:DataTime/BinningInterval)=0.5;
    %%%%% pick diode's timestamps %%%%%% 
%     [~,locs_a2]=findpeaks(diff(a2),'MINPEAKHEIGHT',5*std(diff(a2)));
%      [~,locs]=findpeaks(diff(a_data2),'MINPEAKHEIGHT',50000);
%     analog_loc = (locs_a2)/1000;
%     TimeStamps = cat(2,TimeStamps, analog_loc);   
%     TimeStamps_a2 = analog_loc;
   
%     [b,a] = butter(2,10/1000,'low'); % set butter filter
%     callumin_filter = filter(b,a,callumin);
    
%     y1=callumin_filter(TimeStamps_a2(5)*1000:TimeStamps_a2(6)*1000)';
%  x1 = 0.001:0.001:length(y1)/1000;
y1=a_data(3,TimeStamps(1)*20000:TimeStamps(2)*20000)/10000;
    x1 = 1/20000:1/20000:length(y1)/20000;  
%%%%%%%%%%%%%%%%%%%%%%%%%  Plot Different Trials   %%%%%%%%%%%%%%%%%
%     if length(TimeStamps2)<=18
%         sweepend=length(TimeStamps2);
%     else
%         sweepend=24;
%     end
    sweepend=trend;
    figure(2);
    set(gcf,'position',[150,30,1024,900])
    h = subplot(sweepend+1,1,1);
    sweepend=20;
    for sweepindex=1:sweepend
        TimeStamps2(21)=TimeStamps2(20)+5.5;
        TimeStampsSweep=TimeStamps2(sweepindex:sweepindex+1); % forcus on which trails 
        cut_spikes = seperate_trials(Spikes,TimeStampsSweep);      
        for i = 1:60
            [n,xout] = hist(cut_spikes{i},BinningTime) ;
            BinningSpike(sweepindex,i,:) = n ;
        end

        subplot(sweepend+1,1,sweepindex);
        s=squeeze(sum(BinningSpike(sweepindex,chst1:chend1,:),2));
        a=100;b=250;
        [spks,slocs]=findpeaks(s(a:b),'minpeakdistance',b-a-1);
        statistime(sweepindex,m)=(slocs+a-2)*BinningInterval*1000;
        statispks(sweepindex,m)=spks/30*200;
        plot(BinningTime,s);
hold on;plot(statistime(sweepindex,m)/1000,spks,'*')
         
    end     
        
    subplot(sweepend+1,1,sweepindex+1);
    plot(x1,y1,'r');
    samexaxis('abc','xmt','on','ytac','join','yld',1) % function need ''parseArgs'' function, from matlab center
%     ylim([min(y1)-0.005,max(y1)+0.005]);
%         h = get(gcf,'children');
    set(get(h,'title'),'string',[name,'  ch',num2str(chst1),' to ',num2str(chend1)]);
%     set(get(h,'title'),'string',[name,'  ch',num2str(ch')]);

%     saveas(gcf,[name,'_trials.jpg'],'jpg');
%     saveas(gcf,[name,'_trials.fig'],'fig'); 
    
 %%%%%%%%%%%%%%%%% raster plot %%%%%%%%%%%%%%%%%%%
    BinningSpike2 = sum(BinningSpike(trst:trend,:,:),1);
%     SB=sum(BinningSpike2(1,ch,:),2);
    SB=sum(BinningSpike2(1,chst1:chend1,:),2);
    SB1=squeeze(SB);   
 
    figure(1)
    imagesc(BinningTime,[1:60],squeeze(BinningSpike2));
    title([name,'(sum over ',sprintf('%.0f',length(TimeStamps2)),' trails) ']);  
    xlabel('t(s)');ylabel('channel');
    colorbar;
%     saveas(gcf,[name,'_raster.jpg'],'jpg')
%     saveas(gcf,[name,'_raster.fig'],'fig')     
%%%%%%%%%%%%%  get peaks  %%%%%%%%%%%%%%%%%%%%%
      SB1=SB1/trnum*200/(chend1-chst1+1);
    [spks2,slocs2]=findpeaks(SB1,'minpeakdistance',floor(DataTime/BinningInterval/f),'MINPEAKHEIGHT',p);

    if isempty(slocs2)
        slocstime2=NaN;
    end
    slocstime2=(slocs2-1)*BinningInterval;
    
    if isempty(spks2)==1 
        spks2=0;
    end
     if isempty(slocs2)==1 
        slocs2=0;
    end
    
%%%%%%%%%%%%%%%%%%%%%%%%%% consider delay time %%%%%%%%%%%%%%%%%
%     ondelay=0.28;offdelay=0.12;
%     slocstimedelay=[];
% 
%     for i=1:length(slocstime)
%         if slocstime(i)<=1+ondelay
%             slocstimedelay(i)=slocstime(i)-ondelay;
%         else
%             slocstimedelay(i)=slocstime(i)-offdelay; 
%         end
%     end
%     
    
%%%%%%%%%%%%%%    statistics    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5    
    slocstime2=(slocs2(1)-1)*BinningInterval;
%     slocstimedelay(slocstimedelay<0) = slocstimedelay(slocstimedelay<0)+DataTime;
%     slocsdelay=ceil(slocstimedelay/BinningInterval+1);
    
%     slocslumin=y1(ceil(slocstime*1000));
    slocslumin= (max-min)/delT*slocstime2+min;
%%%%%%%%%%%%%%%%%%%%%%%%%  Plot histogram   %%%%%%%%%%%%%%%%% 
    figure(3)
    subplot(6,1,1:5); 
    plot(BinningTime,SB1,slocstime2,spks2(1),'r*'); 
    
%      title(['T=', sprintf('%.3f',DataTime),'s','   Ch',num2str(chst1),' to ',num2str(chend1),'   Trail ',num2str(trst),' to',num2str(trend_now),sprintf('\n'),...
%             ' PeakTime(s)=',sprintf('%8.3f',slocstime),sprintf('\n'),'PeakPhase(pi)=',sprintf('%8.3f',theta),sprintf('\n'),'Weber Fraction=',sprintf('%8.3f',wf),sprintf('\n'),'WFdelay=',sprintf('%8.3f',wfdelay),...
%             sprintf('\n'),'dePeakTime(s)=',sprintf('%8.3f',slocstimedelay),sprintf('\n'),'dePeakPhase(pi)=',sprintf('%8.3f',thetadelay),...
%             );
    title([name,'   Ch',num2str(chst1),' to ',num2str(chend1),'   Trail ',num2str(trst),' to',num2str(trend),sprintf('\n'),' PeakTime(s)=',sprintf('%8.3f',slocstime2),sprintf('\n'),'light intensity =',sprintf('%8.5f',slocslumin),sprintf('\n'),'firinf rate =',sprintf('%8.3f',spks(1))]);

% title([name,'   Ch',num2str(chst1),' to ',num2str(chend1),'   Trail ',num2str(trst),' to',num2str(trend),sprintf('\n'),' PeakTime(s)=',sprintf('%8.3f',slocstime),sprintf('\n'),'firinf rate =',sprintf('%8.3f',spks(1)),]);
%     ylabel('firing rate per 5ms per trial');
    ylabel(['firing rate per 5ms over ',num2str(trnum),' trials']);
    subplot(6,1,6);
    plot(x1,y1,'r');
%     ylim([min(y1)-0.005,max(y1)+0.005]);
    samexaxis('abc','xmt','on','ytac','join','yld',1) % function need ''parseArgs'' function, from matlab center
    xlabel('t(s)');ylabel('diode(nA)');
  

%     saveas(gcf,[name,'_Ch',num2str(chst1),'to',num2str(chend1),'_Tr',num2str(trst),'to',num2str(trend),'_hist.jpg'],'jpg')
%     saveas(gcf,[name,'_Ch',num2str(chst1),'to',num2str(chend1),'_Tr',num2str(trst),'to',num2str(trend),'_hist.fig'],'fig')
               

%     save(filename,'TimeStamps2','x1','y1','slocstime','min','max','-append'); % save data into original dat

    
%     catch
%         [msgstr,msgerr] = lasterr;
%         disp([msgstr,msgerr])
%     end
       
%%%%%%%%%  focus on other channels   %%%%%%%%%%%%%%    
%     clearvars spks slocs slocstime theta 
%     SB2=sum(BinningSpike(c:d,:))/length(TimeStamps);    
%     [spks,slocs]=findpeaks(SB2,'minpeakdistance',floor(length(BinningTime)/4),'MINPEAKHEIGHT',0.2);   
%     slocstime=(slocs-1)*BinningInterval;    
%     theta=slocs/length(BinningTime)*2; %firing phase(unit:pi)
%      wf=(y(slocs(1)+1)-y(slocs(1)-1))/(2*BinningInterval)/((y(slocs(1)+1)+y(slocs(1)-1))/2);  %%weber's fraction
%     figure
%     plot(BinningTime,SB2,slocstime,spks,'r*');
%     title(['T=', sprintf('%.3f',DataTime),'s','    Ch',num2str(c),' to ',num2str(d),sprintf('\n'),' PeakTime(s)=',sprintf('%8.3f',slocstime),sprintf('\n'),...
%         'PeakPhase(pi)=',sprintf('%8.3f',theta),sprintf('\n'),'Weber Fraction=',sprintf('%.3f',wf)]);
%     xlabel('t(s)');ylabel('spike# in 60 channels within 5ms');
%     saveas(gcf,['T=', num2str(DataTime),'s',' Ch',num2str(a),'to',num2str(b),'_hist.jpg'],'jpg')
%     saveas(gcf,['T=', num2str(DataTime),'s',' Ch',num2str(a),'to',num2str(b),'_hist.fig'],'fig')  
      test(m,chst1) = spks(1); 
    end
% end