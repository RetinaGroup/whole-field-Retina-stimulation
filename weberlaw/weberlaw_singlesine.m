% Analysis single sine wave stimulation, by Rona, 2016
clear all
close all
clc

try
cd('E:\google_rona\20160410\sine_diffT') ; % the folder of the files
catch
cd('C:\Users\jofan\Google ���ݵw��\20160731\sine') ; % the folder of the files    
end % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir.
n_file = length(all_file) ; 

%%%%%%%%%%%%%%   user's setting  %%%%%%%%%%%%%%%%%%%%%%
BinningInterval=0.005;
SamplingRate=20000;
chst1=37;chend1=57;%focus on channel chst1 to chend1
chst2=43;chend2=48;
trst=2;trnum=20; %sweep (trnum) trails from (trst)
f=20;p=0.3;

for m = 5
    clearvars -except f p all_file n_file m BinningInterval SamplingRate chst1 chend1 chst2 chend2 trst trnum statispks statistime
    file = all_file(m).name ;
    [pathstr, name, ext] = fileparts(file);
    directory = [pathstr,'\'];
    filename = [name,ext];
    load([filename]);
    name(name=='_')='-';
%     load(['C:\Users\Hydrochabu\Google ���ݵw��\20160724\diode\','diode_',filename]);
%     
%%%%%%%%%%%%%%%%%%%%%  TimeStamps  %%%%%%%%%%%%%%%%%%%
%     if size(a_data,1)==1              %only find one analog channel, possibly cause by the setting in MC_rack
%         a_data2 = a_data(1,:);
%     else
%         a_data2 = a_data(3,:);   % analog=A3:a_data(3,:) ;analog=A2:a_data(2,:) 
%     end
%     a_data2 = a_data2 - a_data2(1);
%     [b,a] = butter(2,5/10000,'low'); % set butter filter
%     FilterData = filter(b,a,a_data2);
% 
%     [~,locs]=findpeaks(FilterData,'minpeakdistance',10000,'MINPEAKHEIGHT',700);
%     analog_loc = (locs)/SamplingRate;
%     TimeStamps2 = analog_loc(2:30);   
   
%%%%%%%%%%%%%%%%%%%%%%%%%%  Binning  %%%%%%%%%%%%%%%%%%%%%%%%%
    TimeStamps2=TimeStamps(1:1:length(TimeStamps)); %
    
    if length(TimeStamps2)<=(trst+trnum-1)
        trnum = length(TimeStamps2)-trst+1;
    end
    trend=trst+trnum-1;
 
    DataTime = (TimeStamps2(2) - TimeStamps2(1));
    T=TimeStamps2(4)-TimeStamps2(3)-5;

    cut_spikes = seperate_trials(Spikes,TimeStamps2(trst:trend));    

    BinningTime = [ 0 : BinningInterval : DataTime];
    
    %%%%%% a3 %%%%%%%%%
    x1 = 0:BinningInterval:T;
    y1=[]; 
    y1=sin(2*pi*x1/T); %(unit:nA)
%     y1(1/BinningInterval+1:DataTime/BinningInterval)=0.5;
    %%%%% pick diode's timestamps %%%%%% 
%     [~,locs_a2]=findpeaks(diff(diff(a2)),'MINPEAKHEIGHT',5*std(diff(diff(a2))));
% % %      [~,locs]=findpeaks(diff(a_data2),'MINPEAKHEIGHT',50000);
%     analog_loc = (locs_a2)/1000;
% % %     TimeStamps = cat(2,TimeStamps, analog_loc);   
%     TimeStamps_a2 = analog_loc(1:2:length(analog_loc))+0.001;
% %    
%     [b,a] = butter(2,20/1000,'low'); % set butter filter
%     callumin_filter = filter(b,a,callumin);
% %     
%     y1=callumin_filter(TimeStamps_a2(9)*1000:TimeStamps_a2(10)*1000)';
%     x1 = 0.001:0.001:length(y1)/1000;
%     figure;plot(x1,y1)
%     T = TimeStamps_a2(2)-TimeStamps_a2(1);
%%%%%%%%%%%%%%%%%%%%%%%%%  Plot Different Trials   %%%%%%%%%%%%%%%%%
%     if length(TimeStamps2)<=18
%         sweepend=length(TimeStamps2);
%     else
%         sweepend=24;
%     end
    sweepend=trend;
%     figure(2);
%     set(gcf,'position',[150,30,1024,900])
%     h = subplot(sweepend+1,1,1);

    TimeStamps2(21)=TimeStamps2(20)+6;
    for sweepindex=2:sweepend
        TimeStampsSweep=TimeStamps2(sweepindex:sweepindex+1); % forcus on which trails 
        cut_spikes = seperate_trials(Spikes,TimeStampsSweep);      
        for i = 1:60
            [n,xout] = hist(cut_spikes{i},BinningTime) ;
            BinningSpike(sweepindex,i,:) = n ;
        end
        subplot(sweepend+1,1,sweepindex);
%         plot(BinningTime,squeeze(sum(BinningSpike(sweepindex,chst1:chend1,:),2)));       
        s=squeeze(sum(BinningSpike(sweepindex,chst1:chend1,:),2));
        a=100;b=300;
        [spks,slocs]=findpeaks(s(a:b),'minpeakdistance',b-a-1);
        statistime(sweepindex-1,m)=(slocs+a-2)*BinningInterval*1000;
        statispks(sweepindex-1,m)=spks/(chend1-chst1+1)*200;
        plot(BinningTime,s);
        hold on;plot(statistime(sweepindex-1,m)/1000,spks,'*')
        
        
    end     
        
    subplot(sweepend+1,1,sweepindex+1);
    plot(x1,y1,'r');
    samexaxis('abc','xmt','on','ytac','join','yld',1) % function need ''parseArgs'' function, from matlab center
%     ylim([min(y1)-0.005,max(y1)+0.005]);
%     set(get(h,'title'),'string',[name,'  ch',num2str(chst1),' to ',num2str(chend1)]);

%     saveas(gcf,[name,'_trials.jpg'],'jpg');
%     saveas(gcf,[name,'_trials.fig'],'fig'); 
    
 %%%%%%%%%%%%%%%%% raster plot %%%%%%%%%%%%%%%%%%%
    BinningSpike2 = sum(BinningSpike(trst:trend,:,:),1);
%     BinningSpike2 = sum(BinningSpike(8:9,:,:),1);
%     SB=sum(BinningSpike2(1,ch,:),2);
    SB=sum(BinningSpike2(1,chst1:chend1,:),2);
    SB1=squeeze(SB)/trnum/(chend1-chst1+1)*200;   
 
    figure
    imagesc(BinningTime,[1:60],squeeze(BinningSpike2));
% colormap(gray);
% scatter(BinningTime,[1:60],squeeze(BinningSpike2));
    title([name,'(sum over ',sprintf('%.0f',length(TimeStamps2)),' trails) ']);  
%     title([name,'  ( trial 8th )']);  
    xlabel('t(s)');ylabel('channel');
    colorbar;
%     saveas(gcf,[name,'_raster.jpg'],'jpg')
%     saveas(gcf,[name,'_raster.fig'],'fig')     
%%%%%%%%%%%%%  get peaks  %%%%%%%%%%%%%%%%%%%%%
    sm_SB1=smooth(SB1,0.01);
%     SB1=sum(BinningSpike(chst1:chend1,:),1)/trnum;      
    [spks,slocs]=findpeaks(sm_SB1,'minpeakdistance',floor(DataTime/BinningInterval/f),'MINPEAKHEIGHT',p);    

    if isempty(slocs)
        slocstime=NaN;
    end
    
    theta=zeros;
    wf=zeros;
    rate=zeros;
    slocstime=(slocs-1)*BinningInterval;
    slocs_a3=slocs;
%     slocs_a3=floor(slocstime*1000);
%     rate=(y1(slocs_a3)-y1(slocs_a3-1))/0.001;
    
    for i=1:length(slocstime) 
        if slocstime(i)<=T 
            theta(i)=(slocstime(i)/T*2);%firing phase(unit:pi)
            wf(i)=(y1(slocs_a3(i))-y1(slocs_a3(i)-1))./((y1(slocs_a3(i))+y1(slocs_a3(i)-1))/2)/(0.001);  %%weber's fraction % samplingrate of LED=1000
        else
            theta(i)=NaN;
            wf(i)=NaN;
        end
    end
%%%%%%%%%%%%%%%%%%%%%%%%%% consider delay time %%%%%%%%%%%%%%%%%
    ondelay=0.1;offdelay=0.14;
    slocstimedelay=[];
    for i=1:length(slocstime)
        if slocstime(i)<=0.25*T+ondelay || slocstime(i)>=0.75*T+offdelay
            slocstimedelay(i)=slocstime(i)-ondelay;
        else
            slocstimedelay(i)=slocstime(i)-offdelay; 
        end
    end
    
%%%%%%%%%%%%%%%%%%%    statistics    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     slocstimedelay(slocstimedelay<0) = slocstimedelay(slocstimedelay<0)+DataTime;
%     thetadelay=zeros;
%     wfdelay=zeros;
%     slocsdelay=ceil(slocstimedelay/BinningInterval+1);
%     slocsdelay_a3 = slocsdelay;
%     slocsdelay_a3 = floor(slocstimedelay*1000);
%     ratedelay=(y1(slocsdelay_a3)-y1(slocsdelay_a3-1))/0.001;
    
%     for i=1:length(slocstime)
%         if slocstimedelay(i)<=T
%             thetadelay(i)=slocstimedelay(i)/T*2;%firing phase(unit:pi)
%             wfdelay(i)=(y1(slocsdelay_a3(i))-y1(slocsdelay_a3(i)-1))./((y1(slocsdelay_a3(i))+y1(slocsdelay_a3(i)-1))/2)/(0.001);  %%weber's fractio
%         else
%             thetadelay(i)=NaN;
%             wfdelay(i)=NaN;
%         end
%     end
    
     
        
%%%%%%%%%%%%%%%%%%%%%%%%%  Plot histogram   %%%%%%%%%%%%%%%%%        
    figure(3)
    subplot(6,1,2:5); 
    plot(BinningTime,SB1,slocstime,spks,'r*'); 
%     plot(BinningTime,SB1,slocstime,spks,'r*',BinningTime,sm_SB1,'g'); 
%     title(['T=', sprintf('%.3f',T),'s','   Ch',num2str(chst1),' to ',num2str(chend1),'   Trail ',num2str(trst),' to',num2str(trend),sprintf('\n'),...
%            'PeakTime(s)=',sprintf('%8.3f',slocstime),'     dePeakTime(s)=',sprintf('%8.3f',slocstimedelay),sprintf('\n')...
%            'PeakPhase(pi)=',sprintf('%8.3f',theta),'     dePeakPhase(pi)=',sprintf('%8.3f',thetadelay),sprintf('\n')...
%            'Weber Fraction=',sprintf('%8.3f',wf),'     WFdelay=',sprintf('%8.3f',wfdelay),sprintf('\n')...
%            'Rate(Hz)=',sprintf('%8.3f',rate),'     deRate(Hz)=',sprintf('%8.3f',ratedelay)]);           
    ylabel('firing rate per 5ms per trail');
    subplot(6,1,6);
    plot(x1,y1,'r');
    xlabel('t(s)');
    samexaxis('abc','xmt','on','ytac','join','yld',1)
    
 
   
%     saveas(gcf,[name,'_Ch',num2str(chst1),'to',num2str(chend1),'_Tr',num2str(trst),'to',num2str(trend),'_hist.jpg'],'jpg')
%     saveas(gcf,[name,'_Ch',num2str(chst1),'to',num2str(chend1),'_Tr',num2str(trst),'to',num2str(trend),'_hist.fig'],'fig')
%       
%     
%     save(filename,'T','slocstime','slocstimedelay','wf','wfdelay','theta','thetadelay','rate','ratedelay','-append'); % save data into original dat

    
%     catch
%         [msgstr,msgerr] = lasterr;
%         disp([msgstr,msgerr])
%     end
       
%%%%%%%%%  focus on other channels   %%%%%%%%%%%%%%    
%     clearvars spks slocs slocstime theta 
%     SB2=sum(BinningSpike(c:d,:))/length(TimeStamps);    
%     [spks,slocs]=findpeaks(SB2,'minpeakdistance',floor(length(BinningTime)/4),'MINPEAKHEIGHT',0.2);   
%     slocstime=(slocs-1)*BinningInterval;    
%     theta=slocs/length(BinningTime)*2; %firing phase(unit:pi)
%      wf=(y(slocs(1)+1)-y(slocs(1)-1))/(2*BinningInterval)/((y(slocs(1)+1)+y(slocs(1)-1))/2);  %%weber's fraction
%     figure
%     plot(BinningTime,SB2,slocstime,spks,'r*');
%     title(['T=', sprintf('%.3f',DataTime),'s','    Ch',num2str(c),' to ',num2str(d),sprintf('\n'),' PeakTime(s)=',sprintf('%8.3f',slocstime),sprintf('\n'),...
%         'PeakPhase(pi)=',sprintf('%8.3f',theta),sprintf('\n'),'Weber Fraction=',sprintf('%.3f',wf)]);
%     xlabel('t(s)');ylabel('spike# in 60 channels within 5ms');
%     saveas(gcf,['T=', num2str(DataTime),'s',' Ch',num2str(a),'to',num2str(b),'_hist.jpg'],'jpg')
%     saveas(gcf,['T=', num2str(DataTime),'s',' Ch',num2str(a),'to',num2str(b),'_hist.fig'],'fig')  
        
end