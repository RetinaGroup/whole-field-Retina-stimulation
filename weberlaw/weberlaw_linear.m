% Analysis linearly increasing/decreasing intensity, by Rona,2016
clear all
close all
clc

cd('D:\Rona\weberlaw\20160130\tri') ; % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir.
n_file = length(all_file) ; 

%%%%%%%%%%%%%%   user's setting  %%%%%%%%%%%%%%%%%%%%%%
BinningInterval=0.005;
SamplingRate=20000;
chst1=1;chend1=60;%focus on channel chst1= to chend1
chst2=38;chend2=45;
trst=2;trnum=20; %sweep trail from trst to trend
f=40;p=0.8;

for m =13
    clearvars -except all_file n_file m BinningInterval SamplingRate chst1 chend1 chst2 chend2 trst trnum f p
    file = all_file(m).name ;
    [pathstr, name, ext] = fileparts(file);
    directory = [pathstr,'\'];
    filename = [name,ext];
    load([filename]);
    name(name=='_')='-';
    
%%%%%%%%%%%%%%%%%%%%%%  TimeStamps  %%%%%%%%%%%%%%%%%%%
%     if size(a_data,1)==1              %only find one analog channel, possibly cause by the setting in MC_rack
%         a_data2 = a_data(1,:);
%     else
%         a_data2 = a_data(3,:);   % analog=A3:a_data(3,:) ;analog=A2:a_data(2,:) 
%     end
% %     a_data2 = a_data2 - a_data2(1);
%     [~,locs]=findpeaks(diff(a_data2),'MINPEAKHEIGHT',5*std(diff(a_data2)));
%     analog_loc = (locs)/SamplingRate;
%     TimeStamps = cat(2,TimeStamps, analog_loc);   

%%%%%%%%%%%%%%%%%%%%%%%%%%  Binning  %%%%%%%%%%%%%%%%%%%%%%%%%
    TimeStamps2=TimeStamps(1:2:length(TimeStamps)); %if there are 2 timestamps in one trail
    if length(TimeStamps2)<=(trst+trnum-1)
        trnum = length(TimeStamps2)-trst+1;
    end
    trend=trst+trnum-1;
 
    DataTime = (TimeStamps2(2) - TimeStamps2(1));
%     deltaT = 3;
    deltaT = TimeStamps(2)-TimeStamps(1);
    
    cut_spikes = seperate_trials(Spikes,TimeStamps2(trst:trend));    

    BinningTime = [ 0 : BinningInterval : DataTime];
    BinningSpike = zeros(60,length(BinningTime));
    for i = 1:60
        [n,xout] = hist(cut_spikes{i},BinningTime) ;
        BinningSpike(i,:) = n ;
    end
    

    x1 = 0:BinningInterval:DataTime;
    y1=[]; 
    y1(1:deltaT/BinningInterval)=(500/deltaT)*x1(1:deltaT/BinningInterval)+1800-1000; %consider voltage offset=1v, below is the same
    y1(deltaT/BinningInterval:(5+deltaT)/BinningInterval+1)=2300-1000;
    y1((5+deltaT)/BinningInterval+1:(5+2*deltaT)/BinningInterval)=2300-(500/deltaT)*x1(1:deltaT/BinningInterval)-1000;
    y1((5+2*deltaT)/BinningInterval:DataTime/BinningInterval+1)=1800-1000;
%     x = [1/SamplingRate:1/SamplingRate:DataTime+1/SamplingRate];
%     x = [1/SamplingRate:1/SamplingRate:DataTime];
%     y = 1/10000*a_data(2,TimeStamps2(1)*SamplingRate:TimeStamps2(2)*SamplingRate); 
%%%%%%%%%%%%%%%%% raster plot %%%%%%%%%%%%%%%%%%%
    figure(1)
    imagesc(BinningTime,[1:60],BinningSpike);
    title([name,'(sum over ',sprintf('%.0f',length(TimeStamps2)),' trails) ']);  
    xlabel('t(s)');ylabel('channel');
    colorbar;
    saveas(gcf,[name,'_raster.jpg'],'jpg')
    saveas(gcf,[name,'_raster.fig'],'fig')   
    
%%%%%%%%%%%%%%%%%%%%%%%%%  Plot Different Trials   %%%%%%%%%%%%%%%%%
    if length(TimeStamps2)<=15
        sweepend=length(TimeStamps2);
    else
        sweepend=15;
    end
    figure(2);
    set(gcf,'position',[150,30,1024,900])
    h = subplot(sweepend+1,1,1);
    
    for sweepindex=1:sweepend-1
        TimeStampsSweep=TimeStamps2(sweepindex:sweepindex+1); % forcus on which trails 
        cut_spikes = seperate_trials(Spikes,TimeStampsSweep);      
        for i = 1:60
            [n,xout] = hist(cut_spikes{i},BinningTime) ;
            BinningSpike2(i,:) = n ;
        end
%         temp{sweepindex} = BinningSpike2;
        subplot(sweepend+1,1,sweepindex);
        plot(BinningTime,sum(BinningSpike2(chst1:chend1,:),1));
    end     
        
    subplot(sweepend+1,1,sweepindex+1);
    plot(x1,y1,'r');
%     plot(TimeStamps(1:2)-TimeStamps(1), ones(1,length(2-1+1)),'r^')
    samexaxis('abc','xmt','on','ytac','join','yld',1) % function need ''parseArgs'' function, from matlab center
%         h = get(gcf,'children');
    set(get(h,'title'),'string',[name,'  ch',num2str(chst1),' to ',num2str(chend1)]);
%     set(get(h,'title'),'string',[name,'  ch',num2str(chst1),' to ',num2str(chend1)],'units','normalized','position',[0.5,1.0]);  

    saveas(gcf,[name,'_trials.jpg'],'jpg');
    saveas(gcf,[name,'_trials.fig'],'fig');  
    
%%%%%%%%%%%%%  get peaks  %%%%%%%%%%%%%%%%%%%%%   
%     SB1=sum(BinningSpike(chst1:chend1,:),1);
    SB1=sum(BinningSpike(chst1:chend1,:),1)/trnum;
    sm_SB1=smooth(SB1,0.01);
    [spks,slocs]=findpeaks(sm_SB1,'minpeakdistance',floor(DataTime/BinningInterval/f),'MINPEAKHEIGHT',p);    

    if isempty(slocs)
        slocstime=NaN;
    end
    slocstime=(slocs-1)*BinningInterval;
%%%%%%%%%%%%%%%%%%%%%%%%%% consider delay time %%%%%%%%%%%%%%%%%
    ondelay=0.055;offdelay=0.035;
    slocstimedelay=[];

    for i=1:length(slocstime)
        if slocstime(i)<=0.5*DataTime
            slocstimedelay(i)=slocstime(i)-ondelay;
        else
            slocstimedelay(i)=slocstime(i)-offdelay; 
        end
    end
    
    
%%%%%%%%%%%%%%    statistics    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5    
    slocstime=(slocs-1)*BinningInterval;
    slocstimedelay(slocstimedelay<0) = slocstimedelay(slocstimedelay<0)+DataTime;
    slocsdelay=ceil(slocstimedelay/BinningInterval+1);            
    wf=(y1(slocs)-y1(slocs-1))./((y1(slocs)+y1(slocs-1))/2)/(2*BinningInterval);  % weber's fraction 
    wfdelay=(y1(slocsdelay)-y1(slocsdelay-1))./((y1(slocsdelay)+y1(slocsdelay-1))/2)/(BinningInterval);  % weber's fraction
    L=y1(slocs);
    Ldelay=y1(slocsdelay);
    
%%%%%%%%%%%%%%%%%%%%%%%%%  Plot histogram   %%%%%%%%%%%%%%%%%        
    figure(3)
    subplot(6,1,2:5); 
    plot(BinningTime,SB1,slocstime,spks,'r*',BinningTime,sm_SB1,'g'); 
%      title(['T=', sprintf('%.3f',DataTime),'s','   Ch',num2str(chst1),' to ',num2str(chend1),'   Trail ',num2str(trst),' to',num2str(trend_now),sprintf('\n'),...
%             ' PeakTime(s)=',sprintf('%8.3f',slocstime),sprintf('\n'),'PeakPhase(pi)=',sprintf('%8.3f',theta),sprintf('\n'),'Weber Fraction=',sprintf('%8.3f',wf),sprintf('\n'),'WFdelay=',sprintf('%8.3f',wfdelay),...
%             sprintf('\n'),'dePeakTime(s)=',sprintf('%8.3f',slocstimedelay),sprintf('\n'),'dePeakPhase(pi)=',sprintf('%8.3f',thetadelay),...
%             );
   
    title([name,'   Ch',num2str(chst1),' to ',num2str(chend1),'   Trail ',num2str(trst),' to',num2str(trend),sprintf('\n'),' PeakTime(s)=',sprintf('%8.3f',slocstime),sprintf('\n'),'dePeakTime(s)=',sprintf('%8.3f',slocstimedelay),sprintf('\n'),'Weber Fraction=',sprintf('%8.3f',wf),sprintf('\n'),'WFdelay=',sprintf('%8.3f',wfdelay),sprintf('\n'),'Lumin=',sprintf('%5.0f',L),'   LuminDelay=',sprintf('%5.0f',Ldelay)]);
    ylabel('firing rate per 5ms per trial');
    subplot(6,1,6);
    plot(x1,y1,'r');
%     plot(Time(1:length(TimeStamps(1)*20000:TimeStamps(2)*20000)),1800+Filter_a_data(TimeStamps(1)*20000:TimeStamps(2)*20000),'r');
%     t2=TimeStamps2(2);t1=TimeStamps2(1)
%     time=[1/SamplingRate:1/SamplingRate:length(a_data(round(t1*20000):round(t2*20000)))/20000];
   
    
%     plot(time,1/700*a_data(round(t1*20000):round(t2*20000)),'r');
%     plot(BinningTime,SB1,slocstime,spks,'r*');
%     axis([0 DataTime 0 max(SB1)]);
    xlabel('t(s)');
  
   
    saveas(gcf,[name,'_Ch',num2str(chst1),'to',num2str(chend1),'_Tr',num2str(trst),'to',num2str(trend),'_hist.jpg'],'jpg')
    saveas(gcf,[name,'_Ch',num2str(chst1),'to',num2str(chend1),'_Tr',num2str(trst),'to',num2str(trend),'_hist.fig'],'fig')
               

    save(filename,'TimeStamps2','L','Ldelay','x1','y1','slocstime','slocstimedelay','wf','wfdelay', '-append'); % save data into original dat

    
%     catch
%         [msgstr,msgerr] = lasterr;
%         disp([msgstr,msgerr])
%     end
       
%%%%%%%%%  focus on other channels   %%%%%%%%%%%%%%    
%     clearvars spks slocs slocstime theta 
%     SB2=sum(BinningSpike(c:d,:))/length(TimeStamps);    
%     [spks,slocs]=findpeaks(SB2,'minpeakdistance',floor(length(BinningTime)/4),'MINPEAKHEIGHT',0.2);   
%     slocstime=(slocs-1)*BinningInterval;    
%     theta=slocs/length(BinningTime)*2; %firing phase(unit:pi)
%      wf=(y(slocs(1)+1)-y(slocs(1)-1))/(2*BinningInterval)/((y(slocs(1)+1)+y(slocs(1)-1))/2);  %%weber's fraction
%     figure
%     plot(BinningTime,SB2,slocstime,spks,'r*');
%     title(['T=', sprintf('%.3f',DataTime),'s','    Ch',num2str(c),' to ',num2str(d),sprintf('\n'),' PeakTime(s)=',sprintf('%8.3f',slocstime),sprintf('\n'),...
%         'PeakPhase(pi)=',sprintf('%8.3f',theta),sprintf('\n'),'Weber Fraction=',sprintf('%.3f',wf)]);
%     xlabel('t(s)');ylabel('spike# in 60 channels within 5ms');
%     saveas(gcf,['T=', num2str(DataTime),'s',' Ch',num2str(a),'to',num2str(b),'_hist.jpg'],'jpg')
%     saveas(gcf,['T=', num2str(DataTime),'s',' Ch',num2str(a),'to',num2str(b),'_hist.fig'],'fig')  
        
end