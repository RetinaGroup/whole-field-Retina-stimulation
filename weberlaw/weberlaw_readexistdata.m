% Draw statistics results for Weber's law, by Rona, 2018
clear all
close all
clc

cd('C:\Users\jofan\Dropbox\hydrolab\expdata\20160114\single_sine') ; % the folder of the files
all_file = dir('*.mat') ; % change the type of the files which you want to select, subdir or dir.
n_file = length(all_file) ; 

%%%%%%%%%%%%%%   user's setting  %%%%%%%%%%%%%%%%%%%%%%
BinningInterval=0.005;
SamplingRate=20000;
chst1=30;chend1=60;%focus on channel chst1= to chend1
chst2=38;chend2=45;
trst=3;trend=15; %sweep trail from trst to trend

for m = 1
    clearvars -except all_file n_file m BinningInterval SamplingRate chst1 chend1 chst2 chend2 trst trend 
    file = all_file(m).name ;
    [pathstr, name, ext] = fileparts(file);
    directory = [pathstr,'\'];
    filename = [name,ext];
    load([filename]);
    name(name=='_')='-';
   
%%%%%%%%%%%%%%%%%%%%%%%%%%  Binning  %%%%%%%%%%%%%%%%%%%%%%%%%
    trend_now = trend;
    if length(TimeStamps)<=trend_now
        trend_now = length(TimeStamps);
    end
%       TimeStamps2=TimeStamps(trst:trend_now); %%% forcus on which trails
    TimeStamps2=TimeStamps(1:2:length(TimeStamps)); %%(1:2:length(TimeStamps) for if there are 2 timestamps in one trail
    
    DataTime = (TimeStamps2(3) - TimeStamps2(2));
    
    T = (TimeStamps(4)-TimeStamps(3));
    
    cut_spikes = seperate_trials(Spikes,TimeStamps2);    

    BinningTime = [ 0 : BinningInterval : DataTime];
    BinningSpike = zeros(60,length(BinningTime));
    for i = 1:60
        [n,xout] = hist(cut_spikes{i},BinningTime) ;
        BinningSpike(i,:) = n ;
    end
    
    x1 = 0:BinningInterval:DataTime;
    y1=[]; 
    y1 = 1*((sin(2*pi*x1/DataTime)+1)/2+1.8)-1; %% stimulus:M=2.3V;m=1.3V,consider voltage offset=1v 
    
    x = [1/SamplingRate:1/SamplingRate:DataTime+1/SamplingRate];
    y = 1/10000*a_data(3,TimeStamps2(2)*SamplingRate:TimeStamps2(3)*SamplingRate); 
 
%%%%%%%%%%%%%%%%% raster plot %%%%%%%%%%%%%%%%%%%
    figure(1)
    imagesc(BinningTime,[1:60],BinningSpike);
    name(name=='_')=' ';
    title([name]);
    title(['T=', sprintf('%.3f',T),'s', '(sum over trails)  Trail ',num2str(trst),' to',num2str(trend)]);  
    xlabel('t(s)');ylabel('channel');
% % %   caxis([0 2]); %設定colorbar的最大和最小值
    colorbar;
    saveas(gcf,[name,'_raster.jpg'],'jpg')
    saveas(gcf,[name,'_raster.fig'],'fig')   
    
%%%%%%%%%%%%%%%%%%%%%%%%%  Plot Different Trials   %%%%%%%%%%%%%%%%%
    if length(TimeStamps2)<10
        sweepend=length(TimeStamps2);
    else
        sweepend=10;
    end
    figure(2);
    set(gcf,'position',[150,30,1024,900])
    h = subplot(sweepend+1,1,1);
    for sweepindex=1:sweepend
        TimeStampsSweep=TimeStamps2(sweepindex:min(length(TimeStamps2),sweepindex+1) ); % forcus on which trails 
        cut_spikes = seperate_trials(Spikes,TimeStampsSweep);      
        for i = 1:60
            [n,xout] = hist(cut_spikes{i},BinningTime) ;
            BinningSpike2(i,:) = n ;
        end

        subplot(sweepend+1,1,sweepindex);
        plot(BinningTime,sum(BinningSpike2));
    end     
        
    subplot(sweepend+1,1,sweepindex+1);
    plot(x,y,'r')
%     plot(TimeStamps(1:2)-TimeStamps(1), ones(1,length(2-1+1)),'r^')
    samexaxis('abc','xmt','on','ytac','join','yld',1) % function need ''parseArgs'' function, from matlab center
%         h = get(gcf,'children');
    name(name=='_')=' ';
    set(get(h,'title'),'string',[name,sprintf('\n'),'different sweeps (sum 60 channels of a sweep)'],'units','normalized','position',[0.5,1.0]);  

    saveas(gcf,[name,'_trials.jpg'],'jpg');
    saveas(gcf,[name,'_trials.fig'],'fig');  
    
%%%%%%%%%%%%%  get peaks  %%%%%%%%%%%%%%%%%%%%%   
    SB1=sum(BinningSpike(chst1:chend1,:))/length(TimeStamps2);      
    [spks,slocs]=findpeaks(SB1,'minpeakdistance',floor(DataTime/BinningInterval/15),'MINPEAKHEIGHT',2);    

    if isempty(slocs)
        m
        continue
    end
    
    slocstime=(slocs-1)*BinningInterval;    
    theta=(slocstime/DataTime*2);%firing phase(unit:pi)
    
%%%%%%%%%%%%%%%%%%%%%%%%%% consider delay time %%%%%%%%%%%%%%%%%
    ondelay=0.09;offdelay=0.125;
    slocstimedelay=[];
    for i=1:length(slocstime)
        if slocstime(i)<=0.25*DataTime+ondelay || slocstime(i)>=0.75*DataTime+offdelay
            slocstimedelay(i)=slocstime(i)-ondelay;
        else
            slocstimedelay(i)=slocstime(i)-offdelay; 
        end
    end
    
%%%%%%%%%%%%%%%%%%%    statistics    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    slocstimedelay(slocstimedelay<0) = slocstimedelay(slocstimedelay<0)+DataTime;
    thetadelay=slocstimedelay/DataTime*2;%firing phase(unit:pi)
    wf=(y(slocs)-y(slocs-1))./((y(slocs)+y(slocs-1))/2)/(2*BinningInterval);  %%weber's fraction    
    slocsdelay=ceil(slocstimedelay/BinningInterval+1);            
    wfdelay=(y(slocsdelay)-y(slocsdelay-1))./((y(slocsdelay)+y(slocsdelay-1))/2)/(BinningInterval);  %%weber's fractio
     
        
%%%%%%%%%%%%%%%%%%%%%%%%%  Plot histogram   %%%%%%%%%%%%%%%%%        
    figure(3)
    subplot(6,1,2:5); 
    plot(BinningTime,SB1,slocstime,spks,'r*'); 
    title(['T=', sprintf('%.3f',T),'s','   Ch',num2str(chst1),' to ',num2str(chend1),'   Trail ',num2str(trst),' to',num2str(trend_now),sprintf('\n'),...
           ' PeakTime(s)=',sprintf('%8.3f',slocstime),sprintf('\n'),'PeakPhase(pi)=',sprintf('%8.3f',theta),sprintf('\n'),'Weber Fraction=',sprintf('%8.3f',wf),sprintf('\n'),'WFdelay=',sprintf('%8.3f',wfdelay),sprintf('\n'),...
           'dePeakTime(s)=',sprintf('%8.3f',slocstimedelay),sprintf('\n'),'dePeakPhase(pi)=',sprintf('%8.3f',thetadelay)]);  
    ylabel('firing rate per 5ms');
    subplot(6,1,6);
    plot(x,y,'r');
    xlabel('t(s)');
   
    saveas(gcf,[name,'_Ch',num2str(chst1),'to',num2str(chend1),'_Tr',num2str(trst),'to',num2str(trend),'_hist.jpg'],'jpg')
    saveas(gcf,[name,'_Ch',num2str(chst1),'to',num2str(chend1),'_Tr',num2str(trst),'to',num2str(trend),'_hist.fig'],'fig')
      
    save(filename,'T','chst1','chend1','trst','trend','slocstime','slocstimedelay','wf','wfdelay','theta','thetadelay','-append'); % save data into original dat

    
%     catch
%         [msgstr,msgerr] = lasterr;
%         disp([msgstr,msgerr])
%     end
       
%%%%%%%%%  focus on other channels   %%%%%%%%%%%%%%    
%     clearvars spks slocs slocstime theta 
%     SB2=sum(BinningSpike(c:d,:))/length(TimeStamps);    
%     [spks,slocs]=findpeaks(SB2,'minpeakdistance',floor(length(BinningTime)/4),'MINPEAKHEIGHT',0.2);   
%     slocstime=(slocs-1)*BinningInterval;    
%     theta=slocs/length(BinningTime)*2; %firing phase(unit:pi)
%      wf=(y(slocs(1)+1)-y(slocs(1)-1))/(2*BinningInterval)/((y(slocs(1)+1)+y(slocs(1)-1))/2);  %%weber's fraction
%     figure
%     plot(BinningTime,SB2,slocstime,spks,'r*');
%     title(['T=', sprintf('%.3f',DataTime),'s','    Ch',num2str(c),' to ',num2str(d),sprintf('\n'),' PeakTime(s)=',sprintf('%8.3f',slocstime),sprintf('\n'),...
%         'PeakPhase(pi)=',sprintf('%8.3f',theta),sprintf('\n'),'Weber Fraction=',sprintf('%.3f',wf)]);
%     xlabel('t(s)');ylabel('spike# in 60 channels within 5ms');
%     saveas(gcf,['T=', num2str(DataTime),'s',' Ch',num2str(a),'to',num2str(b),'_hist.jpg'],'jpg')
%     saveas(gcf,['T=', num2str(DataTime),'s',' Ch',num2str(a),'to',num2str(b),'_hist.fig'],'fig')  
        
end