% Simulated movie displayed by Weber's law, by Rona, Feb.2016
close all
clear all
load mri
% map=gray(256);
mov = immovie(D,map);

for i=1:128
    for j=1:128
        for k=1:26         
            wb(i,j,1,k) = 8*(D(i,j,1,k+1)-D(i,j,1,k))/(D(i,j,1,k)+1);         
        end
    end
end
wbmov = immovie(wb,map);
implay(mov); % original
implay(wbmov); % weber's law