% dimensionless model of neuronal excitability
v0=0.;w0=0;
a=0.5;b=0;eps=0.1;I0=0;
Y0=[v0,w0];
t=0:0.1:400;
options=odeset('RelTol',1.e-5);
[T, Y]=ode45(@dydt_FHN,t,Y0,options,a,eps,b,I0);
figure(1);plot(T,Y(:,1),T,Y(:,2)); % time courses of V and w
legend('v(t)','w(t)');
xlabel('Time'); ylabel('v, w');

%determine and plot the v,w-nullclines
vpts=(-1.5:.05:1.5);
figure(2);clf;
hold on;
vnullpts=vpts-vpts.^3/3-a+I0;
wnullpts=(vpts+a)/b;
plot(vpts,vnullpts,'black',vpts,wnullpts,'black');
plot_dir(Y(:,1),Y(:,2)); % V-w phase plane 
xlabel('v'); ylabel('w');
% axis([-1 1.5 -.5 1]);

