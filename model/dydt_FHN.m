function dY=dydt_FHN(t,Y,a,eps,b,I0)
v=Y(1);
w=Y(2);
dY=zeros(2,1);
dY(1)=v-v^3-w+I0*1/(1+exp(20-t)/.2);
dY(2)=eps*(v+a-b*w);