clear all
% parameters
tau_y = 0.001;
delay = 100;
k = 0.2;
time_constant = 100;
sumerror = 0;
diff_time = 0;

y = zeros;
error = [];
integrator = [];
diff = [];

% stimulation
Tot = 60;  
dt = 1*10^-3; 
dtau = 1*10^-3; %step width 
T = dtau:dtau:Tot;
G = 5; % damping  
w = G/(2*1.06);%w = G/(2w)=1.06;  
D = 2.6e8; %dynamical range

x = zeros(1,length(T));
V = zeros(1,length(T));
for t = 1:length(T)-1
    x(t+1) = x(t) + V(t)*dt;  %(th*(mu - X(t)))*dt + sqrt(dt)*randn; th = .01;  mu = 3;
    V(t+1) = (1-G*dt)*V(t) - w^2*x(t)*dt + sqrt(dt*D)*randn;
end      
    
% anticipated syncronization
for i = 2:60000
    if i <= delay
        y(i)= y(i-1) + (-y(i-1)/tau_y + x(i-1))*dt;
    else
        error(i) = x(i-1) - y(i-delay)/tau_y; 
%         sumerror = sumerror + error(i)/time_constant
%         integrator(i) = sumerror
%         diff(i) = diff_time*(error(i) - error(i-1))
%         y(i) = y(i-1) + (-y(i-1)/tau_y + x(i-1) + k*error(i) + diff(i))*dt
        y(i) = y(i-1) + (-y(i-1)/tau_y + x(i-1) + k*error(i))*dt;
    end
end

%% MI
backward=ceil(100); forward=ceil(100); % deltat=-1000~1000 ms
isi2 = x(1:6000);
Neurons = y(1:6000);
dat=[];informationp=[];temp=backward+2;
    for i=1:backward+1 %past(t<0)
        a = Neurons((i-1)+forward+1:length(Neurons)-backward+(i-1))';
        b =isi2(forward+1:length(isi2)-backward)';
        dat{i}=[a,b];

        [N,C]=hist3(dat{i}); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
              temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2);
            end
        end
        temp=temp-1;
        informationp(temp)=nansum(temp2(:));
    end  

    dat=[];informationf=[];temp=0;sdat=[];
    for i=1:forward
        a =Neurons(forward+1-i:length(Neurons)-backward-i)';
        b = isi2(forward+1:length(isi2)-backward)';
        dat{i}=[a,b];
        norm=1;

        [N,C]=hist3(dat{i}); %20:dividing firing rate  6:# of stim
        px=sum(N,1)/sum(sum(N)); % x:stim
        py=sum(N,2)/sum(sum(N)); % y:word
        pxy=N/sum(sum(N));
        temp2=[];
        for j=1:length(px)
            for k=1:length(py)
                temp2(k,j)=pxy(k,j)*log( pxy(k,j)/ (py(k)*px(j)) )/log(2);
            end
        end
        temp=temp+1;
        informationf(temp)=nansum(temp2(:)); 
    end
    information=[informationp informationf];
    t=[-backward:forward];  
    figure(21);hold on;plot(t,information,'LineWidth',2);
%     set(gca,'FontSize',16)
    xlabel('\deltat');ylabel('MI (bits)');
    figure;plot(x);hold on;plot(y/tau_y)



        
        
        
        
        
        