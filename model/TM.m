%Reverberation with TM dynamics
%
% 01 - Feb 13, 2015 Created by ckc with simple X0 dynamics put in by hand
% 02 - Feb 14, 2015 added X0 dynamics by leaking

% clear all
% close all
%
% % Initial Conditoins
% E = 0.001 ;x  = 0.8 ;u = 0.3 ;X0 = 0.8 ;
% % parameters
% X00 = 0.8 ;U0  = 0.3 ;J = 4.5 ;a = 1.5 ;tau = 0.013 ;
% tau_D = 0.2 ;tau_F = 1.5 ;tau_x0 = 5. ;I0 = -1.2 ;beta = 0.01 ;
% %
% clearvars -except Spikes TimeStamps
spiketime = Spikes{39};
% Initial Conditoins
x = 0.937 ;u = 0.3 ;X0 = 0.937 ;
% parameters
X00 = 0.95 ;U0  = 0.3 ;J = 5 ;I0 = -1.2 ;a = 1.5 ;
tau = 0.013 ;tau_D = 0.15 ;tau_F = 1.5 ;tau_x0 = 25;
beta = 0.01 ;
%
nstep = 330000. ; Er = zeros(1,nstep) ;ur = Er ;
xr = Er ;time = Er ;X0r = Er ;dt = 0.001 ;
% load('\\192.168.0.100\homes\jofan/spiketime_forTina.mat')
spiketimeindex = fix(spiketime*1000);
spiketime2 = zeros(1,nstep);
spiketime2(spiketimeindex) = 1;

for i = 1 : nstep
	
	dx = ((X0 - x)/tau_D - u*x*spiketime2(i))*dt ;
	du = ((U0 - u)/tau_F + U0*(1-u)*spiketime2(i))*dt ;

    
    x = x + dx ;	u = u + du ;	
    
	xr(i) = x ;	ur(i) = u ;	time(i) = dt*i ;
    ux(i) = u*x;
end
figure(1); subplot(212); plot(time,xr)
xr=ux(TimeStamps(1)*1000:TimeStamps(2)*1000);
Neurons=[];
    states=8;
    X=xr;
    nX = sort(X);
    abin = length(nX)/states;
    intervals = [nX(1:abin:end) inf]; 
    temp=0;
    for jj = 1:10:length(X)
        temp=temp+1;
        Neurons2(temp) = find(X(jj)<intervals,1)-1; % stimulus for every 50ms
    end