
%% generate stimulation sequence
nstep = 600*2*2;
s = zeros(1,nstep); % stimulation

% Step
% s(200:2000) = 1; % step

% HMM
gamma = 100; 
dt = 5*10^-3; %(1./60.)/2
D =  10; %2.6e8
HMM_x = zeros(1,nstep);
v = zeros(1,nstep);
omega = gamma/(2*1.06); % 2*pi*1.5;
HMM_x(1) = 0.1;
v(1) = 0.0;  
for t = 1:nstep-1
    HMM_x(t+1) = HMM_x(t) + v(t)*dt; 
    v(t+1) = (1-gamma*dt)*v(t) - omega^2*HMM_x(t)*dt + sqrt(dt*D)*randn;
end 


% OU
tau = 0.3; %correlation time
dt = 5*10^-3; %(1./60.)/2
x = zeros(1,nstep);
OU_x(1) = 0.1;
for i = 1:nstep-1
    OU_x(i+1) = OU_x(i) + (-OU_x(i)/tau + randn*sqrt(D/dt))*dt;
end



%% smoothing of point i by using average from i-n to i+n a total of 2n+1 points
% sx = smooth(x,5); % yy(3) = (y(1) + y(2) + y(3) + y(4) + y(5))/5
% sv = smooth(v,5);
% figure;hold on
% plot(x,'b');plot(sx,'r');hold off
% figure;hold on
% plot(v,'b');plot(sv,'r');hold off

%% control circuit
ar = zeros(1,nstep); % output
mr = zeros(1,nstep); % hidden control
s = HMM_x/max(HMM_x); % input
% parameters
a0 = 0;
mm = 0;
omega_a = 80; % 40.
omega_m = 10; % 10.
g_m = 10.0/1;
g_s = 5;
a = a0;
m = mm;

% control
for i = 1 : nstep
    da = (-omega_a*(a-a0)+g_m*omega_a*(m-mm) + g_s*omega_a*s(i))*dt;
    dm = (-omega_m*(a-a0)/g_m)*dt;
    a = a+ da;
    m = m + dm;
    ar(i) = a;
    mr(i) = m;
end
figure;hold on;
plot(s,'b');plot(mr,'g');plot(ar/max(ar),'r');
legend('s','mr','ar')

%% Mutual information
[t,mi_xx] = MIfun(s,s);
[t,mi_xa] = MIfun(ar,s); % MIfun(R,S)
[t,mi_va] = MIfun(ar,v);
figure;hold on;plot(t,mi_xx,'b');plot(t,mi_xa,'r');plot(t,mi_va,'g')

figure(100);hold on;plot(t,mi_xa);


%% use vr for prediction
pxr = zeros(1,nstep);
delta = 5.0;

for i = 1:nstep
    pxr(i) = s(i)+ ar(i)*delta/max(ar);
end
figure;hold on;plot(s/max(s),'b');plot(pxr/max(pxr),'g');plot(ar/max(ar),'r')

[t,mi_pxr] = MIfun(pxr,s);
figure;hold on;plot(t,mi_xx,'b');plot(t,mi_xa,'r');plot(t,mi_pxr,'g')









