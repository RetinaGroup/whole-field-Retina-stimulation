%%% Gadanken retina take use of extrapolation, Rona, Nov,2017 %%%

close all
clear all

%% HMM
T = 1:10000;
dt = 1;
G = 0.2; % damping  
w = G/(2*1.06);%w = G/(2w)=1.06;  
D = 1; %dynamical rangedt = 1;
x = zeros(1,length(T));
v = zeros(1,length(T));
for t = 1:length(T)-1
    x(t+1) = x(t) + v(t)*dt;  %(th*(mu - X(t)))*dt + sqrt(dt)*randn; th = .01;  mu = 3;
    v(t+1) = (1-G*dt)*v(t) - w^2*x(t)*dt + sqrt(dt*D)*randn;
end
x = x(2:end);
v = v(2:end);
figure;plot(x);hold on
y = [];
for lag = 6
for t = 1:length(T)-2
    xg = [];vg = []; 
    xg(1) = x(t);
    vg(1) = v(t);
%     vg(1) = (1-G*dt)*vp - w^2*x(t)*dt ;
    
    for i = 1:lag
        xg(i+1) = xg(i) + vg(i)*dt;
        vg(i+1) = (1-G*dt)*vg(i) - w^2*xg(i)*dt;
    end
    y(t) = xg(lag+1);
end
%     plot(y)
    Neurons = y(1:length(T)-50);
    isi2 = x(1:length(T)-50);
    Calculate_MI
end